#!/bin/bash
./analysis_recoHH.py LO_proc_1_mvlq500.0_tagTH1_xsec_2.077.root &
./analysis_recoHH.py LO_proc_1_mvlq650.0_tagTH1_xsec_0.4526.root &
./analysis_recoHH.py LO_proc_1_mvlq800.0_tagTH1_xsec_0.1258.root &
./analysis_recoHH.py LO_proc_1_mvlq1000.0_tagTH1_xsec_0.02856.root &    
./analysis_recoHH.py LO_proc_1_mvlq2000.0_tagTH1_xsec_8.869e-05.root &  
./analysis_recoHH.py LO_proc_1_mvlq3000.0_tagTH1_xsec_8.426e-07.root &

./analysis_recoHH.py NLO_proc_1_mvlq500.0_tagTH1_xsec_2.947.root &
./analysis_recoHH.py NLO_proc_1_mvlq800.0_tagTH1_xsec_0.1744.root &
./analysis_recoHH.py NLO_proc_1_mvlq1000.0_tagTH1_xsec_0.03935.root &   
./analysis_recoHH.py NLO_proc_1_mvlq2000.0_tagTH1_xsec_0.000125.root & 
./analysis_recoHH.py NLO_proc_1_mvlq3000.0_tagTH1_xsec_1.194e-06.root & 

