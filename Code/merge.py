#!/usr/bin/env python
import os, sys, time,math
import ROOT
from ROOT import TH1D
from array import array
from ROOT import TLatex,TPad,TList,TH1,TH1F,TH2F,TH1D,TH2D,TFile,TTree,TCanvas,TLegend,SetOwnership,gDirectory,TObject,gStyle,gROOT,TLorentzVector,TGraph,TMultiGraph,TColor,TAttMarker,TLine,TDatime,TGaxis,TF1,THStack,TAxis,TStyle,TPaveText,TAttFill
import math
import bisect
from optparse import OptionParser
import numpy as np
from ROOT import TTree, TFile, AddressOf, gROOT, TChain, gPad
gStyle.SetOptStat(0000)


#############
# harcode the list of files to merge
#############
inputpath= "/afs/cern.ch/work/a/acarvalh/madanalysisVLQ/event/"
#inputpath=" " 
# for the different processes with VLQ
histos1 = "histos_LO_proc_1_mvlq1000.0_tagTH1_xsec_0.02865.root"
histos2 = "histos_NLO_proc_1_mvlq1000.0_tagTH1_xsec_0.03957.root"
histos3 = " "
histos4 = " " 
histos=[histos1,histos2,histos3,histos4] 
# for the X >hh
histosres1 = "histos_LO_proc_10_mRq700.0_radion_xsecdumb_1.0.root"
histosres2 = "histos_LO_proc_10_mRq700.0_radion_xsecdumb_1.0.root"
histosres3 = " "
histosres4 = " " 
histosres=[histosres1,histosres2,histosres3,histosres4] 
##############
# legend
##############
linestyle = [4,2]
legendtext = ["LO","NLO"]
##################################################################
# Gen level, merge by mass - normalize to XS - stack the processes
###################################################################
print " Merging Gen level "
nSuper =2
# parse XS again
XS = []
for plotSuper in range(0,nSuper) : 
    splited= histos[plotSuper].split('_')
    mass =  splited[4].split('q')
    xss = splited[7].split('.r')
    XS.append(xss)
    order = splited[1]
    process = splited[3]
print "The order is "+str(order)+" the mass is "+str(mass[1])+" GeV, the process is "+str(process)
print len(XS)
nplotsGen =8
histoGenName = ["H1_pt" ,"H1_mass" ,"H2_pt" ,"H2_mass" ,"T1_pt" ,"T1_mass" ,"T2_pt" ,"T2_mass" ]
histoGenX = ["H1_pt" ,"H1_mass" ,"H2_pt" ,"H2_mass" ,"T1_pt" ,"T1_mass" ,"T2_pt" ,"T2_mass" ]
for plotGen in range(0,nplotsGen) :
    c1=ROOT.TCanvas()
    legend = TLegend(.5,.8,1.0,.95)
    if(1>0) :   
    #
        plotSuper=0
        print str(plotSuper)+" "+str(plotGen)+" "+str(linestyle[plotSuper])+" "+str(plotSuper+1)
        name = histos[plotSuper]
        print name
        splited= name.split('_')
        mass =  splited[4].split('q')
        xss1 = splited[7].split('.r')
        print float(xss1[0])
        File = ROOT.TFile.Open(name, 'read')
        histoname = histoGenName[plotGen]
        histodraw = File.Get(histoGenName[plotGen])
        col = plotSuper+1
        histodraw.SetLineColor(1)
        histodraw.SetLineWidth(3)
        st = linestyle[plotSuper]
        histodraw.SetLineStyle(1)
        histodraw.SetTitle(" ")
        histodraw.GetYaxis().SetTitle("diff XS")
        histodraw.GetXaxis().SetTitle(histoGenX[plotGen])
        histodraw.Scale(float(xss1[0])/histodraw.Integral()) # 
        legend.AddEntry(histodraw,legendtext[plotSuper])
        histodraw.Draw("hist") 
        ###########
        plotSuper=1
        print str(plotSuper)+" "+str(plotGen)+" "+str(linestyle[plotSuper])+" "+str(plotSuper+1)
        name = histos[plotSuper]
        print name
        splited= name.split('_')
        mass =  splited[4].split('q')
        xss1 = splited[7].split('.r')
        print float(xss1[0])
        File1 = ROOT.TFile.Open(name, 'read')
        histoname1 = histoGenName[plotGen]
        histodraw1 = File1.Get(histoGenName[plotGen])
        col = plotSuper+1
        histodraw1.SetLineColor(1)
        histodraw1.SetLineWidth(3)
        st = linestyle[plotSuper]
        histodraw1.SetLineStyle(2)
        histodraw1.SetTitle(" ")
        histodraw1.GetYaxis().SetTitle("diff XS")
        histodraw1.GetXaxis().SetTitle(histoGenX[plotGen])
        histodraw1.Scale(float(xss1[0])/histodraw1.Integral()) # 
        legend.AddEntry(histodraw1,legendtext[plotSuper])
        histodraw1.Draw("same,hist")
    legend.Draw("same")
    c1.Print(histoGenName[plotGen]+".png")
    
##################################################################
# Global variables, merge by mass - normalize to XS - stack the processes
###################################################################
nplotsGen =4
histoGenName = ["NJets" ,"NFatJets" ,"NFatJets" ,"NBJets" ]
histoGenX = ["NJets" ,"NFatJets" ,"NFatJets" ,"NBJets"]
for plotGen in range(0,nplotsGen) :
    c1=ROOT.TCanvas()
    legend = TLegend(.5,.8,1.0,.95)
    if(1>0) :   
        #
        plotSuper=0
        print str(plotSuper)+" "+str(plotGen)+" "+str(linestyle[plotSuper])+" "+str(plotSuper+1)
        name = histos[plotSuper]
        print name
        splited= name.split('_')
        mass =  splited[4].split('q')
        xss1 = splited[7].split('.r')
        print float(xss1[0])
        File = ROOT.TFile.Open(name, 'read')
        histoname = histoGenName[plotGen]
        histodraw = File.Get(histoGenName[plotGen])
        col = plotSuper+1
        histodraw.SetLineColor(1)
        histodraw.SetLineWidth(3)
        st = linestyle[plotSuper]
        histodraw.SetLineStyle(1)
        histodraw.SetTitle(" ")
        histodraw.GetYaxis().SetTitle("diff XS")
        histodraw.GetXaxis().SetTitle(histoGenX[plotGen])
        histodraw.Scale(1./histodraw.Integral()) # 
        legend.AddEntry(histodraw,legendtext[plotSuper])
        histodraw.Draw("hist") 
        ###########
        plotSuper=1
        print str(plotSuper)+" "+str(plotGen)+" "+str(linestyle[plotSuper])+" "+str(plotSuper+1)
        name = histos[plotSuper]
        print name
        splited= name.split('_')
        mass =  splited[4].split('q')
        xss1 = splited[7].split('.r')
        print float(xss1[0])
        File1 = ROOT.TFile.Open(name, 'read')
        histoname1 = histoGenName[plotGen]
        histodraw1 = File1.Get(histoGenName[plotGen])
        col = plotSuper+1
        histodraw1.SetLineColor(1)
        histodraw1.SetLineWidth(3)
        st = linestyle[plotSuper]
        histodraw1.SetLineStyle(2)
        histodraw1.SetTitle(" ")
        histodraw1.GetYaxis().SetTitle("diff XS")
        histodraw1.GetXaxis().SetTitle(histoGenX[plotGen])
        histodraw1.Scale(1./histodraw1.Integral()) # 
        legend.AddEntry(histodraw1,legendtext[plotSuper])
        histodraw1.Draw("same,hist")
    legend.Draw("same")
    c1.Print(histoGenName[plotGen]+".png")

##################################################################
# ATLAS recast analysis variables
###################################################################
# merge resonant and QQ - different resonance masses
nplotsATLAS =13
histoATLASName = ["Xhh" ,"histM4jATLAS" ,"histH1ptATLAS" ,"histH2ptATLAS" ,"histH1MATLAS" ,"histH2MATLAS" ,\
                "XhhB" ,"histDetaB", "histMJJATLAS", "histH1JptATLAS", "histH2JptATLAS", "histH1JMATLAS", "histH2JMATLAS" ]
histoATLASX = ["Xhh" ,"histM4jATLAS" ,"histH1ptATLAS" ,"histH2ptATLAS" ,"histH1MATLAS" ,"histH2MATLAS" ,\
             "XhhB" ,"histDetaB", "histMJJATLAS", "histH1JptATLAS", "histH2JptATLAS", "histH1JMATLAS", "histH2JMATLAS" ]
for plotGen in range(0,nplotsATLAS) :
    c1=ROOT.TCanvas()
    legend = TLegend(.5,.8,1.0,.95)
    #
    plotSuper =0
    name = histosres[plotSuper]
    print name
    splited= name.split('_')
    mass =  splited[4].split('q')
    xss1 = splited[7].split('.r')
    print float(xss1[0])
    File = ROOT.TFile.Open(name, 'read')
    histoname = histoATLASName[plotGen]
    histodraw = File.Get(histoATLASName[plotGen])
    col = plotSuper+1
    histodraw.SetLineColor(1)
    histodraw.SetLineWidth(3)
    st = linestyle[plotSuper]
    histodraw.SetLineStyle(1)
    histodraw.SetTitle(" ")
    histodraw.GetYaxis().SetTitle("diff XS")
    histodraw.GetXaxis().SetTitle(histoATLASX[plotGen])
    histodraw.Scale(1./histodraw.Integral()) # 
    legend.AddEntry(histodraw," radion 500 GeV")
    histodraw.Draw("hist") 
    #
    plotSuper =1
    name = histosres[plotSuper]
    print name
    splited= name.split('_')
    mass =  splited[4].split('q')
    xss1 = splited[7].split('.r')
    print float(xss1[0])
    File2 = ROOT.TFile.Open(name, 'read')
    histoname = histoATLASName[plotGen]
    histodraw = File2.Get(histoATLASName[plotGen])
    col = plotSuper+1
    histodraw.SetLineColor(1)
    histodraw.SetLineWidth(3)
    st = linestyle[plotSuper]
    histodraw.SetLineStyle(2)
    histodraw.SetTitle(" ")
    histodraw.GetYaxis().SetTitle("diff XS")
    histodraw.GetXaxis().SetTitle(histoATLASX[plotGen])
    histodraw.Scale(1./histodraw.Integral()) # 
    legend.AddEntry(histodraw," radion 1 TeV")
    histodraw.Draw("hist,same") 
    legend.Draw("same")
    c1.Print(histoATLASName[plotGen]+".png")


# gen level plots compare LO and NLO / staking the diffent components to the 3 production modes
# normalized to the XS

# reco level plots compare 




'''
    histodraw1=[]
    for plotSuper in range(0,nSuper) : 
    #
    #plotSuper=0
    print str(plotSuper)+" "+str(plotGen)+" "+str(linestyle[plotSuper])+" "+str(plotSuper+1)
    name = histos[plotSuper]
    print name
    splited= name.split('_')
    mass =  splited[4].split('q')
    xss = splited[7].split('.r')
    print float(xss[0])
    File = ROOT.TFile.Open(name, 'read')
    histodraw1.append(TH1D(File.Get(histoGenName[plotGen])))
    #histodraw1[plotSuper] = File.Get(histoGenName[plotGen])
    print len(histodraw1)
    #histodraw = ROOT.TH1D() 
    histodraw = histodraw1[0]
    histodraw.Scale(1./49999)
    legend.AddEntry(histodraw,legendtext[plotSuper])
    histodraw.Draw("hist")
    legend.Draw()
    c1.Print(histoGenName[plotGen]+".png")
    
    for plotSuper in range(1,nSuper) :         
    col = plotSuper+1
    histodraw=histodraw1[plotSuper]
    histodraw.SetLineColor(1)
    histodraw.SetLineWidth(3)
    st = linestyle[plotSuper]+1
    histodraw.SetLineStyle(1)
    histodraw.SetTitle(" ")
    histodraw.GetYaxis().SetTitle("diff XS")
    histodraw.GetXaxis().SetTitle(histoGenX[plotGen])
    histodraw.Scale(1/49999) # float(xss[0])
    legend.AddEntry(histodraw,legendtext[plotSuper])
    histodraw.Draw("same,hist")
    
    ''' 