#!/bin/bash
./analysis_recoHH.py NLO_proc_4_mvlq500.0_tagTH1_xsec_0.004478.root &  
./analysis_recoHH.py NLO_proc_4_mvlq650.0_tagTH1_xsec_0.004978.root &
./analysis_recoHH.py NLO_proc_4_mvlq800.0_tagTH1_xsec_0.005233.root &
./analysis_recoHH.py NLO_proc_4_mvlq1000.0_tagTH1_xsec_0.005061.root & 
./analysis_recoHH.py NLO_proc_4_mvlq2000.0_tagTH1_xsec_0.001571.root & 
./analysis_recoHH.py NLO_proc_4_mvlq3000.0_tagTH1_xsec_0.000151.root & 

./analysis_recoHH.py LO_proc_4_mvlq500.0_tagTH1_xsec_0.004169.root &   
./analysis_recoHH.py LO_proc_4_mvlq650.0_tagTH1_xsec_0.004621.root &
./analysis_recoHH.py LO_proc_4_mvlq800.0_tagTH1_xsec_0.004775.root &
./analysis_recoHH.py LO_proc_4_mvlq1000.0_tagTH1_xsec_0.004541.root & 
./analysis_recoHH.py LO_proc_4_mvlq2000.0_tagTH1_xsec_0.001268.root & 
./analysis_recoHH.py LO_proc_4_mvlq3000.0_tagTH1_xsec_0.0001115.root & 


