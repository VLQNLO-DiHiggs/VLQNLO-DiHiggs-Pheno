#!/bin/bash
./analysis_recoHH.py LO_proc_5_mvlq500.0_tagTH1_xsec_6.758e-05.root &
./analysis_recoHH.py LO_proc_5_mvlq650.0_tagTH1_xsec_4.35e-05.root &
./analysis_recoHH.py LO_proc_5_mvlq800.0_tagTH1_xsec_2.772e-05.root &
./analysis_recoHH.py LO_proc_5_mvlq1000.0_tagTH1_xsec_1.522e-05.root & 
./analysis_recoHH.py LO_proc_5_mvlq2000.0_tagTH1_xsec_7.664e-07.root & 
./analysis_recoHH.py LO_proc_5_mvlq3000.0_tagTH1_xsec_4.919e-08.root & 

./analysis_recoHH.py NLO_proc_5_mvlq500.0_tagTH1_xsec_7.636e-05.root &  
./analysis_recoHH.py NLO_proc_5_mvlq650.0_tagTH1_xsec_4.944e-05.root &
./analysis_recoHH.py NLO_proc_5_mvlq800.0_tagTH1_xsec_3.155e-05.root &
./analysis_recoHH.py NLO_proc_5_mvlq1000.0_tagTH1_xsec_1.79e-05.root &  
./analysis_recoHH.py NLO_proc_5_mvlq3000.0_tagTH1_xsec_6.397e-08.root & 
./analysis_recoHH.py NLO_proc_5_mvlq2000.0_tagTH1_xsec_9.691e-07.root & 
