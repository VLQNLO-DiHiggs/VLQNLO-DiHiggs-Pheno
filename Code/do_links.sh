#!/bin/tcsh
find -type l -delete

ln -s ../../delphes_fastjet/delphes/classes/SortableObject.h SortableObject.h
ln -s ../../delphes_fastjet/delphes/classes/DelphesClasses.h DelphesClasses.h
ln -s ../../delphes_fastjet/delphes/classes/                 classes
ln -s ../../delphes_fastjet/delphes/external/ExRootAnalysis/ ExRootAnalysis
ln -s ../../delphes_fastjet/delphes/libDelphes.so            libDelphes.so
ln -s ../../delphes_fastjet/delphes/external/fastjet/        fastjet

cd /afs/cern.ch/work/a/acarvalh/GridPacks/genproductions/bin/MadGraph5_aMCatNLO/CMSSW_8_0_0_pre5/
eval `scramv1 runtime -csh`
cd -
