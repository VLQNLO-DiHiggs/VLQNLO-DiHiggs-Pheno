#!/bin/bash

./analysis_recoHH.py LO_proc_10_mvlq500.0_tagTH1_xsec_1.334e-01.root &
./analysis_recoHH.py LO_proc_10_mvlq650.0_tagTH1_xsec_6.818e-02.root &
./analysis_recoHH.py LO_proc_10_mvlq800.0_tagTH1_xsec_3.798e-02.root &
./analysis_recoHH.py LO_proc_10_mvlq1000.0_tagTH1_xsec_1.874e-02.root &  
./analysis_recoHH.py LO_proc_10_mvlq2000.0_tagTH1_xsec_1.022e-03.root & 
./analysis_recoHH.py LO_proc_10_mvlq3000.0_tagTH1_xsec_8.329e-05.root & 

./analysis_recoHH.py LO_proc_11_mvlq500.0_tagTH1_xsec_1.346e-02.root &   
./analysis_recoHH.py LO_proc_11_mvlq650.0_tagTH1_xsec_5.795e-03.root &
./analysis_recoHH.py LO_proc_11_mvlq800.0_tagTH1_xsec_2.759e-03.root &
./analysis_recoHH.py LO_proc_11_mvlq1000.0_tagTH1_xsec_1.147e-03.root & 
./analysis_recoHH.py LO_proc_11_mvlq2000.0_tagTH1_xsec_3.397e-05.root & 
./analysis_recoHH.py LO_proc_11_mvlq3000.0_tagTH1_xsec_1.935e-06.root & 

./analysis_recoHH.py NLO_proc_11_mvlq500.0_tagTH1_xsec_1.795e-02.root &
./analysis_recoHH.py NLO_proc_11_mvlq800.0_tagTH1_xsec_3.685e-03.root &
./analysis_recoHH.py NLO_proc_11_mvlq650.0_tagTH1_xsec_7.658e-03.root &
./analysis_recoHH.py NLO_proc_11_mvlq1000.0_tagTH1_xsec_1.0.root &       
./analysis_recoHH.py NLO_proc_11_mvlq2000.0_tagTH1_xsec_4.684e-05.root & 
./analysis_recoHH.py NLO_proc_11_mvlq3000.0_tagTH1_xsec_2.735e-06.root &

./analysis_recoHH.py NLO_proc_10_mvlq500.0_tagTH1_xsec_1.771e-01.root &
./analysis_recoHH.py NLO_proc_10_mvlq650.0_tagTH1_xsec_9.040e-02.root &
./analysis_recoHH.py NLO_proc_10_mvlq800.0_tagTH1_xsec_5.000e-02.root &
./analysis_recoHH.py NLO_proc_10_mvlq1000.0_tagTH1_xsec_2.481e-02.root &  
./analysis_recoHH.py NLO_proc_10_mvlq2000.0_tagTH1_xsec_1.390e-03.root &
./analysis_recoHH.py NLO_proc_10_mvlq3000.0_tagTH1_xsec_1.171e-04.root &


