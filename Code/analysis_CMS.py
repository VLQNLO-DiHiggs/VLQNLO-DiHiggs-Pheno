#!/usr/bin/env python

help='''
to run: ./analysis_CMS.py delphes_output.root
'''

import os, sys, math, ROOT
import numpy as np
from optparse import OptionParser 

# Delphes headers
ROOT.gInterpreter.Declare('#include "ExRootAnalysis/ExRootTreeReader.h"')
ROOT.gInterpreter.Declare('#include "classes/DelphesClasses.h"')
ROOT.gSystem.Load("libDelphes")

'''
    proc 1 = p p > Q Qbar (QCD only)"
    proc 2 = p p > Q Qbar (EW  only)"
    proc 3 = p p > Q Qbar (EW-QCD interference - LO when Higgs coupling exists)"
    proc 4 = p p > Q Q (EW only)"
    proc 5 = p p > Qbar Qbar (EW only)"
    proc 6 = p p -> Q + jets"
    proc 7 = p p -> Qbar + jets"
'''

def dihiggs_CMSB2G16008 (branchAK8Jet):
  selected = False 

  p4_ak8_0 = ROOT.TLorentzVector()
  p4_ak8_0.SetPtEtaPhiM(branchAK8Jet.At(0).PT, branchAK8Jet.At(0).Eta,
    branchAK8Jet.At(0).Phi, branchAK8Jet.At(0).Mass)

  p4_ak8_1 = ROOT.TLorentzVector()
  p4_ak8_1.SetPtEtaPhiM(branchAK8Jet.At(1).PT, branchAK8Jet.At(1).Eta,
    branchAK8Jet.At(1).Phi, branchAK8Jet.At(1).Mass)

  if (    p4_ak8_0.Pt() > 300 
      and p4_ak8_1.Pt() > 300 
      and abs(p4_ak8_0.Eta()) < 2.4 
      and abs(p4_ak8_1.Eta()) < 2.4 
      and ( branchAK8Jet.At(0).Tau[1]/branchAK8Jet.At(0).Tau[0] < 0.6) 
      and ( branchAK8Jet.At(1).Tau[1]/branchAK8Jet.At(1).Tau[0] < 0.6) 
      and ( branchAK8Jet.At(0).Prun[0] > 105 and branchAK8Jet.At(0).Prun[1] < 135 ) 
      and ( branchAK8Jet.At(1).Prun[0] > 105 and branchAK8Jet.At(1).Prun[1] < 135 ) 
      and abs(p4_ak8_0.Eta() - p4_ak8_1.Eta()) > 1.3 ) :
    selected = True
  else: selected = False

  if selected == True:
    print branchAK8Jet.At(0).Prun[0] and branchAK8Jet.At(1).Prun[0]

  return selected

###sys.exit()

def main():

  if len(sys.argv) < 1:
      print help
      sys.exit(1)

  infile = sys.argv[1]
  inputFile = os.path.basename(infile)
  inputpath=os.path.dirname(infile)
  outputpath="output"
  
  splited= inputFile.split('_')
  
  order   = splited[0]
  process = splited[2]
  mass    =  float(splited[3][len('mvlq'):])
  xsec    = float('.'.join(splited[6].rsplit('.')[:-1]))
  
  print " Order %s process %s mass %f cross section %f" \
    % (order, process, mass, xsec)

  chain = ROOT.TChain("Delphes")
  chain.Add(infile)
  treeReader = ROOT.ExRootTreeReader(chain)
  nentries         = treeReader.GetEntries() 
  branchEvent     = treeReader.UseBranch("Event")
  branchJet       = treeReader.UseBranch("Jet")
  branchAK8Jet    = treeReader.UseBranch("AK8Jet")
  branchParticle  = treeReader.UseBranch("Particle")
  branchScalarHT  = treeReader.UseBranch("ScalarHT")

  try: sys.argv[2]
  except: maxevts = nentries
  else: 
    if int(sys.argv[2]) > 0: maxevts = int(sys.argv[2])
    else: maxevts = nentries

  for e in range(0, maxevts):

    treeReader.ReadEntry(e)

    weight = branchEvent.At(0).Weight
    nak8 = branchAK8Jet.GetEntries()

    if nak8 < 2: continue

    selected = dihiggs_CMSB2G16008 (branchAK8Jet)
    #print selected

  sys.exit()

if __name__ == "__main__":
  main()
