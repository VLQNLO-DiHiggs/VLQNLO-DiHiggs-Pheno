to run the code you just need: 

./do_links.sh (you just need to do it once, make sure of the delphes paths in this files)

./analysis.py delphes_output.root

===================================================

Conventions:

1) All the signal events need to be in the same “inputpath”

2) The name convention for the file is important, It should be like this:

name.root = NLO_proc_1_mvlq1000.0_tagTH1_xsec_0.03957.root

Note that to make efficiency tables and merge/stack histograms we will use from the <name>

order process massGEV XSec

The ”process” numbering should follow:

print "      - 1: p p > Q Qbar (QCD only)" \
print "      - 2: p p > Q Qbar (EW  only)" \
print "      - 3: p p > Q Qbar (EW-QCD interference - LO when Higgs coupling exists)" \
print "      - 4: p p > Q Q (EW only)" \
print "      - 5: p p > Qbar Qbar (EW only)" \
print "      - 6: p p -> Q + jets" \
print "      - 7: p p -> Qbar + jets" \

just as the generation script, and: 

print "      - 8: p p > Qbar H "
print "      - 9: p p -> HH “   
print "      - 10: p p -> X -> HH”

===============================================

If you do:

./analysis.py name.root

The efficiency tables and histograms will be saved in the “inputpath” directory as:

histos_name.root
efficiency_order_proc_process.txt

If you analyse different masses for the same process and order the table of efficiencies will be added in the same file

If you do:

./merge.py

It should merge the histograms from the files that are hardcoded in that script and make png/pdf figures

=================================================

To run a pre-installed version of pythia standalone (instead of doing the local instalation)

export PYTHIA8DATA=/afs/cern.ch/work/a/acarvalh/madanalysisVLQ/Pythia8/share/Pythia8/xmldoc

To run pythia standalone 

./main41 input.lhe  output.hepmc

