#!/usr/bin/env python
# to run: ./analysis.py delphes_output.root
import os, sys, time,math
import ROOT
#from ROOT import TLatex,TPad,TList,TH1,TH1F,TH2F,TH1D,TH2D,TFile,TTree,TCanvas,TLegend,SetOwnership,gDirectory,TObject,gStyle,gROOT,TLorentzVector,TGraph,TMultiGraph,TColor,TAttMarker,TLine,TDatime,TGaxis,TF1,THStack,TAxis,TStyle,TPaveText,TAttFill,TF2, gPad, TGaxis, TChain,TClass 
from array import array
import numpy as np
pow = ROOT.TMath.Power
import bisect
from optparse import OptionParser
import math

# Delphes headers
ROOT.gInterpreter.Declare('#include "ExRootAnalysis/ExRootTreeReader.h"')
ROOT.gInterpreter.Declare('#include "classes/DelphesClasses.h"')
ROOT.gSystem.Load("libDelphes")
### https://cp3.irmp.ucl.ac.be/projects/delphes/wiki/WorkBook/RootTreeDescription

'''
pt > 30
eta < 2.5 

X^2 = [(mH1 - 115)/sigH]^2 + [(mH2 - 115)/sigH]^2


X < 1

LMR: Pair such |mH1 - 115|< 34
HMR: pair such DR < 1.5
'''

def CosThetaStar(diphoton, dihiggs):
  boost_H = -dihiggs.BoostVector()
  diphoton.Boost(boost_H)
  diphoton_vect = diphoton.Vect().Unit()
  #fCSaxis = CSaxis(dihiggs)
  #cosThetaStar = fCSaxis.Dot(diphoton_vect)
  cosThetaStar = diphoton.CosTheta()
  return cosThetaStar

def deltaPhi(phi1, phi2):
  PHI = abs(phi1-phi2)
  if PHI<=3.1415:
      return PHI
  else:
      return 2*3.1415-PHI
def deltaR(eta1, phi1, eta2, phi2):
  deta = eta1-eta2
  dphi = deltaPhi(phi1,phi2)
  return sqrt(deta*deta + dphi*dphi)
#############################################################
# Loop over all events
#############################################################
def main():
  print "load"
  recoVLQ = False
  recoTruth = False

  parser = OptionParser()
  if len(sys.argv) < 1:
    print " Usage: Example1.py <input_file>"
    sys.exit(1)
  inputFile = sys.argv[1]

  '''
    print "   *** Aborting: the process ID must be an integer number"
    print "      - 1: p p > Q Qbar (QCD only)"
    print "      - 2: p p > Q Qbar (EW  only)"
    print "      - 3: p p > Q Qbar (EW-QCD interference - LO when Higgs coupling exists)"
    print "      - 4: p p > Q Q (EW only)"
    print "      - 5: p p > Qbar Qbar (EW only)"
    print "      - 6: p p -> Q + jets"
    print "      - 7: p p -> Qbar + jets"

    print "      - 10: p p -> Q + H"
    print "      - 11: p p -> Qbar + H"
  '''

  splited= inputFile.split('_')
  mass =  splited[3].split('q')
  XS =splited[6].split('.r')
  order = splited[0]
  process = splited[2]
  print "The order is "+str(order)+" the mass is "+str(mass[1])+" GeV, the process is "+str(process)
  #########################
  # Cuts
  #########################
  Tau21cut = 0
  Tau31cut = 0
  PrunMass2 = 60
  PrunMass3 = 1000
  VLQresolution = 200
  Hresolution=50
  #########################
  # categorization
  #########################
  category = -1 # 2: boosted-boosted / 1: boosted-resolved / 0: resolved-resolved 
  H0jj = -1 # just to see wheater we find a dijet pair
  VLQTag2 = 0
  VLQTag1 = 0
  HTag2 = 0
  HTag2sel = 0
  HTag1 = 0
  HTag1sel = 0
  HTag0 = 0
  H0Tagjj = 0
  HTag0sel =0
  #
  # truth countings
  #
  HTag2truth = 0
  HTag1truth = 0
  HRes1truth = 0
  HRes0truth = 0
  #
  HResHTag1truth = 0
  VLQTag2truth = 0
  VLQTag1truth = 0
  VLQRes1truth = 0
  HTag2VLQtruth = 0
  VLQHTag1truth = 0
  VLQRes0truth = 0
  #
  HTag2noVLQ = 0
  HTag0noVLQ = 0
  HTag1noVLQ = 0
  HTag2noVLQtruth = 0
  HTag0noVLQtruth = 0
  HTag1noVLQtruth = 0
  #
  lost = 0
  #
  # CMS recast
  #
  CMSresolved4b = 0
  CMSresolved2H = 0
  CMSresolvedSigRegion = 0
  #
  CMSboosted = 0
  CMSboosted2J = 0
  #
  # ATLAS recast
  #
  ATLASboosted2J = 0
  ATLASboostedDeta = 0
  ATLASboostedXhh = 0
  #
  ATLASresolved4b = 0 
  ATLASresolvedMass = 0
  ATLASresolvedDijet = 0
  ATLASresolvedSigRegion = 0 
  #
  lenQ=0
  if (int(process) == 1 or int(process) == 2 or int(process) == 3 or int(process) == 4 or int(process) == 5 ) : lenQ = 2
  elif (int(process) == 6 or int(process) == 7 or int(process) == 10 or int(process) == 11 ) : lenQ = 1
  elif (int(process) == 10 ) : lenQ = 0
  else : print "not implemented signal process"
  #########################
  # Create chain of root trees
  chain = ROOT.TChain("Delphes")
  chain.Add(str(inputpath)+str(inputFile))
  # Create object of class ExRootTreeReader
  treeReader = ROOT.ExRootTreeReader(chain)
  #if sys.argv[2] and int(sys.argv[2]) >= 0:
  #numberOfEntries = int(sys.argv[2])
  #else: 
  numberOfEntries =  treeReader.GetEntries() # 100 # 
  print "The tree has "+str(treeReader.GetEntries())+" events"
  print ">>>> Going to run on "+str(numberOfEntries)+" events"
  # Get pointers to branches used in this analysis
  branchEvent = treeReader.UseBranch("Event")
  branchJet = treeReader.UseBranch("Jet")
  branchFatJet = treeReader.UseBranch("AK8Jet")
  branchParticle = treeReader.UseBranch("Particle")
  branchHT = treeReader.UseBranch("ScalarHT")
  #  Electron = treeReader.UseBranch("Electron")
  #############################################################
  # Declare histograms
  #############################################################
  # genvariables
  histH1PT = ROOT.TH1F("H1_pt", "Leading H Gen P_{T}", 100, 0.0, 3000.0)
  histH1Mass = ROOT.TH1F("H1_mass", "Leading Gen M_{H}", 100, 00.0, 140.0)
  histH2PT = ROOT.TH1F("H2_pt", "Sub-leading H Gen P_{T}", 100, 0.0, 3000.0)
  histH2Mass = ROOT.TH1F("H2_mass", "Sub-leading  Gen M_{H}", 100, 00.0, 140.0)
  histH1PTDR = ROOT.TH2F("H1_ptDR", "Leading H Gen P_{T} X DR", 100, 0.0, 3000.0, 21, -0.5, 20.5)
  histH2PTDR = ROOT.TH2F("H2_ptDR", "Sub-leading H Gen P_{T} X DR", 100, 0.0, 3000.0, 21, -0.5, 20.5)
  #
  histT1PT = ROOT.TH1F("T1_pt", "Leading Q Gen P_{T}", 100, 0.0, 3000.0)
  histT1Mass = ROOT.TH1F("T1_mass", "Leading Gen M_{Q}", 100, 400.0, 3000.0)
  histT2PT = ROOT.TH1F("T2_pt", "Sub-leading Q Gen P_{T}", 100, 0.0, 3000.0)
  histT2Mass = ROOT.TH1F("T2_mass", "Sub-leading Gen M_{Q}", 100, 400.0, 3000.0)
  #
  histGenJ1PT = ROOT.TH1F("GenJ1_pt", "Leading GenJ P_{T}", 100, 0.0, 3000.0)
  histGenJ2PT = ROOT.TH1F("GenJ2_pt", "Sub-leading GenJ P_{T}", 100, 0.0, 3000.0)
  #
  # global variables
  #
  histJ1Mass = ROOT.TH1F("J1_mass", "Leading J Mass", 100, 0.0, 3000.0)
  histNJets = ROOT.TH1F("NJets", "# Jets (total)", 11, -0.5, 10.5)
  histHTJets = ROOT.TH1F("HTJets", "HT Jets (total)", 100, 400.0, 3000.0)
  histNFatJets = ROOT.TH1F("NFatJets", "# Fat Jets", 11, -0.5, 10.5)
  histNBJets = ROOT.TH1F("NBJets", "#B Jets", 21, -0.5, 20.5)
  #
  # fat tag control plots - to do
  #
  histFatJ1Mass = ROOT.TH1F("FatJ1_mass", "Leading FatJ Mass", 100, 0.0, 400.0)
  histFatJ1Tau3 = ROOT.TH1F("FatJ1_tau3", "Leading FatJ #tau_{31}", 10, 0.0, 1)
  histFatJ1Tau2 = ROOT.TH1F("FatJ1_tau2", "Leading FatJ #tau_{21}", 10, 0.0, 1)
  #
  # reconstructed masses - summing categories
  #
  histH1RecoMass = ROOT.TH1F("H1_mass_Reco", "Leading M_{H}", 100, 0.0, 400.0)
  histH2RecoMass = ROOT.TH1F("H2_mass_Reco", "Sub-leading  M_{H}", 100, 0.0, 400.0)
  histH1RecoMassCat2 = ROOT.TH1F("H1_mass_Reco_cat2", "Leading M_{H}", 100, 0.0, 400.0)
  histH2RecoMassCat2 = ROOT.TH1F("H2_mass_Reco_cat2", "Sub-leading  M_{H}", 100, 0.0, 400.0)
  histH1RecoMassCat1 = ROOT.TH1F("H1_mass_Reco_cat1", "Leading M_{H}", 100, 0.0, 400.0)
  histH2RecoMassCat1 = ROOT.TH1F("H2_mass_Reco_cat1", "Sub-leading  M_{H}", 100, 0.0, 400.0)
  histH1RecoMassCat0 = ROOT.TH1F("H1_mass_Reco_cat0", "Leading M_{H}", 100, 0.0, 400.0)
  histH2RecoMassCat0 = ROOT.TH1F("H2_mass_Reco_cat0", "Sub-leading  M_{H}", 100, 0.0, 400.0)
  #
  histRedMHHRecoMass = ROOT.TH1F("MHH_Red_mass_Reco", "M_{HH}^{red} (GeV)", 40, 0.0, 3000.0)
  histMHHRecoMass = ROOT.TH1F("MHH_mass_Reco", "M_{HH} (GeV)", 50, 0.0, 4500.0)
  histMHHRecoMassCat2 = ROOT.TH1F("MHH_mass_Reco_cat2", "M_{HH} (GeV)", 50, 0.0, 4500.0)
  histMHHRecoMassCat1 = ROOT.TH1F("MHH_mass_Reco_cat1", "M_{HH} (GeV)", 50, 0.0, 4500.0)
  histMHHRecoMassCat0 = ROOT.TH1F("MHH_mass_Reco_cat0", "M_{HH} (GeV)", 50, 0.0, 4500.0)
  #
  histptHHReco = ROOT.TH1F("PTHH_mass_Reco", "p_t^{HH} (GeV)", 50, 0.0, 2000.0)
  histptHHRecoCat2 = ROOT.TH1F("PTHH_mass_Reco_cat2", "p_t^{HH} (GeV)", 50, 0.0, 2000.0)
  histptHHRecoCat1 = ROOT.TH1F("PTHH_mass_Reco_cat1", "p_t^{HH} (GeV)", 50, 0.0, 2000.0)
  histptHHRecoCat0 = ROOT.TH1F("PTHH_mass_Reco_cat0", "p_t^{HH}(GeV)", 50, 0.0, 2000.0)
  #
  histHTRecoMass = ROOT.TH1F("HT_mass_Reco", "scalar H_{T} (GeV)", 50, 0.0, 4500.0)
  histHTRecoMassCat2 = ROOT.TH1F("HT_mass_Reco_cat2", "scalar H_{T} (GeV)", 50, 0.0, 4500.0)
  histHTRecoMassCat1 = ROOT.TH1F("HT_mass_Reco_cat1", "scalar H_{T} (GeV)", 50, 0.0, 4500.0)
  histHTRecoMassCat0 = ROOT.TH1F("HT_mass_Reco_cat0", "scalar H_{T} (GeV)", 50, 0.0, 4500.0)
  # 
  histDRHHReco = ROOT.TH1F("DRHH_mass_Reco", "#Delta R(H,H)", 70, 0, 5.3)
  histDRHHRecoCat2 = ROOT.TH1F("DRHH_mass_Reco_cat2", "DR(H,H)", 20, 0, 3.5)
  histDRHHRecoCat1 = ROOT.TH1F("DRHH_mass_Reco_cat1", "#Delta R(H,H)", 20, 0, 3.5)
  histDRHHRecoCat0 = ROOT.TH1F("DRHH_mass_Reco_cat0", "#Delta R(H,H)", 20, 0, 3.5)
  # 
  histCostHHReco = ROOT.TH1F("CostHH_mass_Reco", "CostHH", 10, 0, 1)
  histCostHHRecoCat2 = ROOT.TH1F("CostHH_mass_Reco_cat2", "CostHH cat2", 10, 0, 1)
  histCostHHRecoCat1 = ROOT.TH1F("CostHH_mass_Reco_cat1", "CostHH cat1", 10, 0, 1)
  histCostHHRecoCat0 = ROOT.TH1F("CostHH_mass_Reco_cat0", "CostHH cat0", 10, 0, 1)
  # 
  histDetaHHReco = ROOT.TH1F("DetaHH_mass_Reco", "#Delta #eta (H,H)", 50, 0, 5)
  histDMHHReco = ROOT.TH1F("DMHH_mass_Reco", "#Delta M (H,H)", 50, 0, 2)
  histDphiHHReco = ROOT.TH1F("DphiHH_mass_Reco", "#Delta #phi (H,H)", 500, 0, 3.2)
  histDetaHHRecoCat2 = ROOT.TH1F("DetaHH_mass_Reco_cat2", "#Delta #eta (H,H)", 50, 0, 5)
  histDetaHHRecoCat1 = ROOT.TH1F("DetaHH_mass_Reco_cat1", "#Delta #eta (H,H)", 50, 0, 5)
  histDetaHHRecoCat0 = ROOT.TH1F("DetaHH_mass_Reco_cat0", "#Delta #eta (H,H)", 50, 0, 5)
  #
  histNJetsNoH = ROOT.TH1F("NJetsNH", "# Jets (that not Higgses)", 21, -0.5, 20.5)
  # reconstructed VLQ + variables to diferentiate
  histVLQ1RecoMass = ROOT.TH1F("VLQ1_mass_Reco", "Leading M_{Q}", 100, 40.0, 1400.0)
  histVLQ2RecoMass = ROOT.TH1F("VLQ2_mass_Reco", "Sub-leading  M_{Q}", 100, 40.0, 1400.0)
  # 
  # Analysis variables
  #
  ##########################################
  #
  # ATLAS histograms
  #
  histXhh = ROOT.TH1F("Xhh", "Xhh", 51, -0.5, 20.5) # just to check 
  binLowE = [400,450,500,550,600,650,700,750,800,850, 900,950,1000,1150,1500]
  histM4jATLAS = ROOT.TH1F("histM4jATLAS", "histM4jATLAS",14,array('d',binLowE)) # find the variable binning
  histH1jjptATLAS = ROOT.TH1F("histH1ptATLAS", "histH1ptATLAS", 51, 40, 2000) # just to see
  histH2jjptATLAS = ROOT.TH1F("histH2ptATLAS", "histH2ptATLAS", 51, 40, 2000) # just to see
  histH1jjMATLAS = ROOT.TH1F("histH1MATLAS", "histH1MATLAS", 51, 40, 300) # just to see
  histH2jjMATLAS = ROOT.TH1F("histH2MATLAS", "histH2MATLAS", 51, 40, 300) # just to see
  #
  histXhhB = ROOT.TH1F("XhhB", "XhhB", 51, -0.5, 20.5) # just to check 
  histDetaB = ROOT.TH1F("histDetaB", "histDetaB", 25, -0.5, 2.5) # just to check 
  histMJJATLAS = ROOT.TH1F("histMJJATLAS", "histMJJATLAS", 51, 400, 2000) # find the variable binning
  histH1JptATLAS = ROOT.TH1F("histH1JptATLAS", "histH1JptATLAS", 51, 40, 2000) # just to see
  histH2JptATLAS = ROOT.TH1F("histH2JptATLAS", "histH2JptATLAS", 51, 40, 2000) # just to see
  histH1JMATLAS = ROOT.TH1F("histH1JMATLAS", "histH1JMATLAS", 51, 40, 300) # just to see
  histH2JMATLAS = ROOT.TH1F("histH2JMATLAS", "histH2JMATLAS", 51, 40, 300) # just to see
  #
  ##########################################
  #
  # CMS histograms
  #
  histDetaCMS = ROOT.TH1F("histDetaCMS", "histDetaCMS", 51, -0.5, 20.5) # just to check 
  histMJJCMS = ROOT.TH1F("histMJJCMS", "histMJJCMS", 88, 800, 3000) # find the variable binning
  histMJJCMSred = ROOT.TH1F("histMJJCMSred", "histMJJCMS", 88, 800, 3000) # find the variable binning
  #
  histXCMS = ROOT.TH1F("histXCMS", "histXCMS", 20, -0.5, 5.5) # just to check 
  histM4bCMS = ROOT.TH1F("histM4bCMS", "histM4bCMS", 290, 350, 1800) # 
  ###########################################
  negative=0
  timer =0 
  timer2=0
  for entry in range(0, numberOfEntries): # 
    timer+=1
    if (timer==1000) : 
        timer2+=1
        print "events processed: "+str(timer*timer2)
        timer = 0
    # Load selected branches with data from specified event
    treeReader.ReadEntry(entry)
    weight = branchEvent.At(0).Weight
    #print weight
    #print "entry : "+str(entry)
    if(weight < 0 ) : 
        negative+=1
        continue
    '''
    #####################
    # Gen-level particles 
    #- it is wrong, taking the LHE information, we should use the genJets instead
    #####################
    Higgses = []
    Topone = []
    GenJets = []
    Genb = []
    Genbbar = []
    QQ = True
    DRGEN =0
    if (QQ) :
        statusH = -1 # 1 #52# 22#52
        statusT = -1 #62 #62
        statusGenJ = -1 #62
    #print "entries " +str(branchParticle.GetEntries())
    for part in range(0, branchParticle.GetEntries()):
       genparticle =  branchParticle.At(part)
       pdgCode = genparticle.PID
       IsPU = genparticle.IsPU
       status = genparticle.M2 # genparticle.Status    
       if(IsPU == 0 and (pdgCode == 25)): # and status==statusH and abs(motherPID)  > 600000): 
          mother =  branchParticle.At(genparticle.M1)
          motherPID = mother.PID
          #print "H mother: "+str(motherPID)
          if(branchParticle.At(genparticle.D1).PID != 25 and branchParticle.At(genparticle.D1).PT > 0) : 
             Higgses.append(genparticle) # find other way to follow
             if(int(process) < 10 and abs(branchParticle.At(genparticle.D1).PID)!=pdgCode ) :
                if (entry ==0):
                   print "H decay: "+str(branchParticle.At(genparticle.D1).PID)
                   print "H decay 2: "+str(branchParticle.At(genparticle.D2).PID)
                dumb = ROOT.TLorentzVector()
                dumb.SetPtEtaPhiM(branchParticle.At(genparticle.D1).PT,branchParticle.At(genparticle.D1).Eta,branchParticle.At(genparticle.D1).Phi,branchParticle.At(genparticle.D1).Mass)
                Genb.append(dumb)
                dumb2 = ROOT.TLorentzVector()
                dumb2.SetPtEtaPhiM(branchParticle.At(genparticle.D2).PT,branchParticle.At(genparticle.D2).Eta,branchParticle.At(genparticle.D2).Phi,branchParticle.At(genparticle.D2).Mass) 
                Genbbar.append(dumb2) 
                DRGEN=dumb2.DeltaR( dumb )
       if (IsPU == 0 and (abs(pdgCode) > 6000000)): #and status==statusT ): 
          if(abs(branchParticle.At(genparticle.D1).PID) != abs(pdgCode) and len(Topone) <3) : 
             Topone.append(genparticle) # the LHE information...
             #print "Q decay: "+str(branchParticle.At(genparticle.D1).PID)
             #print "Q decay 2: "+str(branchParticle.At(genparticle.D2).PID)
       if (IsPU == 0 and (abs(pdgCode) == 5)): # and branchParticle.At(genparticle.M1).PID ==25):# there is no, Jet.Flavour works
          GenJets.append(genparticle)
          #print "b mother: "+str(genparticle.M1)
          #mother =  branchParticle.At(genparticle.M1)
          #motherPID = mother.PID
    if (len(Higgses) ==2) :
       # sort by pt = weird!
       if (Higgses[1].PT > Higgses[0].PT): 
          genparticle = Higgses[0]
          Higgses[0] = Higgses[1]
          Higgses[1] = genparticle
       if (len(Genb)>1 and len(Genbbar)>1 ): 
          histH1PTDR.Fill( Higgses[0].PT, Genb[0].DeltaR(Genbbar[0]))
          histH2PTDR.Fill( Higgses[1].PT,  Genb[1].DeltaR(Genbbar[1]))
       histH1PT.Fill(Higgses[0].PT )
       histH2PT.Fill(Higgses[1].PT )
       histH1Mass.Fill(Higgses[0].Mass )
       histH2Mass.Fill(Higgses[1].Mass )
    else : print "not two Higgses " + str(len(Higgses))
    if (len(Topone) ==2) : # if there are 2
       if (Topone[1].PT > Topone[0].PT): 
          genparticle = Topone[0]
          Topone[0] = Topone[1]
          Topone[1] = genparticle    
       histT1PT.Fill(Topone[0].PT )
       histT2PT.Fill(Topone[1].PT )
       histT1Mass.Fill(Topone[0].Mass )
       histT2Mass.Fill(Topone[1].Mass )
    elif (len(Topone) ==1) :   
       histT1PT.Fill(Topone[0].PT )
       histT2PT.Fill(-100 )
       histT1Mass.Fill(Topone[0].Mass )
       histT2Mass.Fill(-100 )
    #####################
    # Gen-Jets
    # it is wrong, taking the LHE information, we should use the genJets insteaddo not have btag 
    #####################
    # Jet.Flavour works
    '''
    #####################################################
    # Large-R for ATLAS boosted recast - those are sorted by PT 
    # Apply ATLAS algo - but do not remove from the other collections TOFIX
    #####################################################
    '''
    RecoFatJets2ATLAS = []
    for part in range(0, branchFatJet.GetEntries()): # add one more collection to the delphes card
        jet =  branchFatJet.At(part) # take the trimed jet 
        if( jet.PT > 250 and jet.PT < 1500 and abs(jet.Eta) < 2 and jet.Mass > 50 ) :
           dumb = ROOT.TLorentzVector()
           dumb.SetPtEtaPhiM(jet.PT,jet.Eta,jet.Phi,jet.Mass)
           RecoFatJets2ATLAS.append(dumb)
    '''
    ####################################################
    # Jets for signal categorization: We construct three excludent vectors of jets
    # 1) CMS-like fat jets (by now also for VLQ tag)
    # 2) b-tagged jets: also used for the ATLAS resolved recast
    # 3) non-btagged jets
    ####################################################
    #####################
    # Fat Jets - those are sorted by PT 
    # CMS algo applied 
    #####################
    #print "FatJet properties"
    RecoFatJets3 = [] # forget about it 
    RecoFatJets2 = []
    RecoFatJetsATLAS = []
    RecoFatJets2Mass = []
    RecoFatJetsATLASMass = []
    #print dihiggs_CMSB2G16008 (branchFatJet)
    for part in range(0, branchFatJet.GetEntries()):
        jet =  branchFatJet.At(part)
        dumb = ROOT.TLorentzVector()
        dumb.SetPtEtaPhiM(jet.PT,jet.Eta,jet.Phi,jet.Mass)
        JMass = jet.Prun[0] # it should be SoftDroppedP4
        histFatJ1Mass.Fill(JMass)
        if (    dumb.Pt() > 200 and abs(dumb.Eta()) < 2.4 and (jet.Prun[0] > 105 and jet.Prun[0] < 135  ) ):
           if(branchFatJet.At(0).Tau[0] >0) :            
               tau21 = jet.Tau[1]/jet.Tau[0]
               histFatJ1Tau2.Fill(tau21)
               histFatJ1Tau3.Fill(branchFatJet.At(0).Tau[2]/branchFatJet.At(0).Tau[0])
               if ( tau21 > Tau21cut ) :
                  if abs(dumb.Eta()) < 2.0 and dumb.M() > 50 and dumb.Pt() < 1500 and dumb.Pt() > 250  : 
                     RecoFatJetsATLAS.append(dumb)
                     RecoFatJetsATLASMass.append(JMass)        
                  if (  1>0  ) :
                     RecoFatJets2.append(dumb)
                     RecoFatJets2Mass.append(JMass)        
    #####################
    # Jets - those are sorted by PT 
    # making sure does do not match the FatJets
    #####################
    tolerancefat = 0.8
    tolerance = 0.4
    RecoJets = []
    RecoAllJets = []
    RecoJetsBTag = []
    RecoJetsBTagCMS = []
    for part in range(0, branchJet.GetEntries()):
        jet =  branchJet.At(part)
        dumb = ROOT.TLorentzVector()
        dumb.SetPtEtaPhiM(jet.PT,jet.Eta,jet.Phi,jet.Mass)
        test = 0
        for ii in range(0, len(RecoFatJets2)):
          if ( dumb.DeltaR(RecoFatJets2[ii]) < tolerancefat ) : test = -1
        if ( test == 0 and jet.BTag == 1 and jet.PT >40 and abs(jet.Eta) <2.5 ) : RecoJetsBTag.append(dumb)
        elif ( test == 0  and jet.PT >30 and abs(jet.Eta) <5.0) : RecoJets.append(dumb)
        if ( test == 0 and jet.PT >30 and abs(jet.Eta) <5.0 ) : RecoAllJets.append(dumb)
        if ( test == 0 and jet.BTag == 1 and jet.PT >30 and abs(jet.Eta) <2.5 ) : RecoJetsBTagCMS.append(dumb) 
        if (part==0) : 
            histJ1Mass.Fill(jet.Mass)
    ########################################################
    # Global variables
    ########################################################
    histNJets.Fill(branchJet.GetEntries())
    histHTJets.Fill(branchHT.At(0).HT) 
    histNBJets.Fill(len(RecoJetsBTagCMS))
    histNFatJets.Fill(len(RecoFatJets2))
    costhh=-10
    costhh2=-10
    ########################################################
    # Algoritm for recast resolved
    ########################################################
    if(len(RecoJetsBTag) > 3) :
        #print "resolved ATLAS"
        ################################
        # ATLAS
        ################################
        ATLASresolved4b+=1
        passcut = 0
        # the jets are already organized by Pt
        counter = 0
        counter2 = 0
        mjj1 = -10
        mjj2 = -10
        ptjj1 = -10
        ptjj2 = -10
        etajj1 = -10
        etajj2 = -10
        for jj in range(0,len(RecoJetsBTag)):
          for kk in range(0,len(RecoJetsBTag)):
            if (kk != jj) :
              for ii in range(0,len(RecoJetsBTag)):
                if (ii != kk and ii!= jj) :
                  for yy in range(0,len(RecoJetsBTag)):
                    if (yy!=ii and yy != kk and yy!= jj) :
                      if ( RecoJetsBTag[jj].DeltaR(RecoJetsBTag[kk]) < 1.5 and RecoJetsBTag[ii].DeltaR(RecoJetsBTag[yy]) < 1.5) : 
                        if ((RecoJetsBTag[jj]+RecoJetsBTag[kk]).Pt() > (RecoJetsBTag[ii]+RecoJetsBTag[yy]).Pt()) :
                            j1 = (RecoJetsBTag[jj]+RecoJetsBTag[kk])
                            j2 = (RecoJetsBTag[ii]+RecoJetsBTag[yy])
                        else :   
                            j2 = (RecoJetsBTag[jj]+RecoJetsBTag[kk])
                            j1 = (RecoJetsBTag[ii]+RecoJetsBTag[yy])
                            counter+=1 # take only the first one , no ambiguities
                        if (counter == 0 and j1.Pt() > 200 and j2.Pt() >150) : 
                            mjj1 = j1.M()
                            mjj2 = j2.M()
                            ptjj1 = j1.Pt()
                            ptjj2 = j2.Pt()
                            counter2+=1
        #if(counter>0) : print "there was ambiguity"
        #if (counter==0) : print "opsy, made no pairs" 
        if (counter2==0) : passcut = 1
        if(passcut==0) : ATLASresolvedDijet+=1
        PHH = (RecoJetsBTag[0]+RecoJetsBTag[1]+RecoJetsBTag[2]+RecoJetsBTag[3])
        m4b = PHH.M()
        ### leading jet
        if(m4b > 910) : 
           if ( ptjj1 < 400 ) :  passcut = 1
        elif(m4b < 600) : 
           if ( ptjj1 < 200 ) :  passcut = 1
        else : 
           if ( ptjj1 < 0.65*m4b-190 ) :  passcut = 1
        ### subleading jet
        if(m4b > 990) : 
            if ( ptjj2 < 400 ) :  passcut = 1
        elif(m4b < 520) : 
            if ( ptjj2 < 200 ) :  passcut = 1
        else : 
            if ( ptjj2 < 0.23*m4b+30 ) :  passcut = 1
        ### angle
        if(m4b < 810) :
            if ( abs(etajj1 - etajj1)< 1.0 and etajj1!=-10) :  passcut = 1
        else :
            if ( abs(etajj1 - etajj1)< 0.0016*m4b-0.28 and etajj1!=-10 ) :  passcut = 1
        # We ignore the ttbar veto (90% efficiency)
        if (passcut==0) : 
           ATLASresolvedMass+=1
           Xhh = math.sqrt( ((mjj1-124)**2/(0.1*mjj1)**2) + ((mjj2-115)**2/(0.1*mjj2)**2)) 
           #histXhh.Fill(Xhh)
           if (Xhh < 1.6 ) : 
               ATLASresolvedSigRegion+=1
               histM4jATLAS.Fill(m4b)
               histH1jjptATLAS.Fill(ptjj1)
               histH2jjptATLAS.Fill(ptjj2)
               histH1jjMATLAS.Fill(mjj1)
               histH2jjMATLAS.Fill(mjj2)
    ########################################################
    # Algoritm for recast CMS resolved  
    ########################################################
    if(len(RecoJetsBTagCMS) > 3) :
        #print "resolved CMS"
        CMSresolved4b+=1
        passcut = 0
        # # the jets are already organized by Pt
        counter = 0
        mjj1 = -10
        mjj2 = -10
        ptjj1 = -10
        ptjj2 = -10
        etajj1 = -10
        etajj2 = -10
        m4b = -10
        XHMR = -10
        XLMR = -10
        sigH = 23
        counter = 0
        for jj in range(0,len(RecoJetsBTagCMS)):
          for kk in range(0,len(RecoJetsBTagCMS)):
            if (kk != jj) :
              for ii in range(0,len(RecoJetsBTagCMS)):
                if (ii != kk and ii!= jj) :
                  for yy in range(0,len(RecoJetsBTagCMS)):
                    if (yy!=ii and yy != kk and yy!= jj) :
                      # HMR
                      if ( counter==0 and RecoJetsBTagCMS[jj].DeltaR(RecoJetsBTagCMS[kk]) < 1.5 and RecoJetsBTagCMS[ii].DeltaR(RecoJetsBTagCMS[yy]) < 1.5) : 
                        if ((RecoJetsBTagCMS[jj]+RecoJetsBTagCMS[kk]).Pt() > (RecoJetsBTagCMS[ii]+RecoJetsBTagCMS[yy]).Pt()) :
                            j1 = (RecoJetsBTagCMS[jj]+RecoJetsBTagCMS[kk])
                            j2 = (RecoJetsBTagCMS[ii]+RecoJetsBTagCMS[yy])
                        else :   
                            j2 = (RecoJetsBTagCMS[jj]+RecoJetsBTagCMS[kk])
                            j1 = (RecoJetsBTagCMS[ii]+RecoJetsBTagCMS[yy])
                        counter+=1 # take only the first one , no ambiguities
        #if(counter>0) : print "there was ambiguity"
        #if (counter==0) : print "opsy, made no pairs" 
        #if (counter==0) : passcut = 1
        if(counter==1) : 
           CMSresolved2H+=1
           mjj1 = j1.M() 
           mjj2 = j2.M()
           ptjj1 = j1.Pt()
           ptjj2 = j2.Pt()
           m4b = (j1 + j2).M()
           XHMR = math.sqrt(((mjj1 - 115)/sigH)**2 + ((mjj2 - 115)/sigH)**2)
           histXCMS.Fill(XHMR) # just to check
           if(XHMR<1.0) : 
              CMSresolvedSigRegion+=1 
              histM4bCMS.Fill(m4b) 
    ########################################################
    # Algoritm for categorization 
    ########################################################
    VLQ1 = ROOT.TLorentzVector()
    VLQ2 = ROOT.TLorentzVector()
    H1 = ROOT.TLorentzVector()
    H2 = ROOT.TLorentzVector()
    MH1 = -100
    MH2 = -100
    PTHH = -100
    MHH = -100
    HT = -100
    NJET = -100
    DRHH = -100
    DetaHH = -100
    DMHH = -100
    DphiHH = -100
    recoHtag=0
    ###############
    # ATLAS boosted
    ###############
    if(len(RecoFatJetsATLAS)>1) :
      #print "2 tag"
      category = 2 
      # https://github.com/delphes/delphes/blob/master/cards/delphes_card_CMS_PileUp.tcl#L796-L802
      btagweight0 = (0.85*math.tanh(0.0025*RecoFatJetsATLAS[0].Pt()/2)*(25.0/(1+0.063*RecoFatJetsATLAS[0].Pt()/2)))**2
      btagweight1 = (0.85*math.tanh(0.0025*RecoFatJetsATLAS[1].Pt()/2)*(25.0/(1+0.063*RecoFatJetsATLAS[1].Pt()/2)))**2
      HTag2+=1*btagweight0*btagweight1
      if(RecoFatJetsATLAS[0].Pt() >350 ):
        ATLASboosted2J+=1*btagweight0*btagweight1
        Deta = abs(RecoFatJetsATLAS[0].Eta()-RecoFatJetsATLAS[1].Eta())
        XhhB = math.sqrt( ((RecoFatJetsATLASMass[0]-124)**2/(0.1*RecoFatJetsATLASMass[0])**2) + ((RecoFatJetsATLASMass[1]-115)**2/(0.1*RecoFatJetsATLASMass[1])**2))
        #histDetaHH.Fill(Deta)
        if (Deta < 1.7) :
           ATLASboostedDeta+=1*btagweight0*btagweight1 
           histXhhB.Fill(XhhB)
           if(XhhB < 1.6 ) : 
              ATLASboostedXhh+=1*btagweight0*btagweight1
    ###############
    # 2 H tag
    ###############
    if(len(RecoFatJets2)>1) :
      #print "2 tag"
      category = 2 
      # https://github.com/delphes/delphes/blob/master/cards/delphes_card_CMS_PileUp.tcl#L796-L802
      btagweight0 = (0.85*math.tanh(0.0025*RecoFatJets2[0].Pt()/2)*(25.0/(1+0.063*RecoFatJets2[0].Pt()/2)))**2
      btagweight1 = (0.85*math.tanh(0.0025*RecoFatJets2[1].Pt()/2)*(25.0/(1+0.063*RecoFatJets2[1].Pt()/2)))**2
      HTag2+=1*btagweight0*btagweight1
      ########################################################
      # CMS 
      ########################################################
      DetaCMS = (RecoFatJets2[0].Eta() - RecoFatJets2[1].Eta())
      histDetaCMS.Fill(DetaCMS)
      CMSboosted2J+=1*btagweight0*btagweight1
      if(abs(DetaCMS) < 1.3 )  :
         CMSboosted+=1*btagweight0*btagweight1
         histMJJCMS.Fill((RecoFatJets2[0] + RecoFatJets2[1]).M())
         HTag2sel+=1*btagweight0*btagweight1
         histMJJCMSred.Fill((RecoFatJets2[0] + RecoFatJets2[1]).M()- RecoFatJets2[0].M() + RecoFatJets2[1].M() +250)
      ########################################################
      # Categories
      ########################################################
      #if(XhhB < 1.6 ) : 
      HTag2sel+=1*btagweight0*btagweight1
      recoHtag=1
      H1 = RecoFatJets2[0]
      H2 = RecoFatJets2[1]
      MH1 = RecoFatJets2Mass[0] # it is the prunned mass
      MH2 = RecoFatJets2Mass[1] # it is the prunned mass
      PHH = (H1 + H2)
      costhh = abs(CosThetaStar(H1, PHH))
      costhh2 = abs(CosThetaStar(H2, PHH))
      PTHH = PHH.Pt()
      MHH = PHH.M()
      DRHH = H2.DeltaR( H1 ) 
      DetaHH = abs(H1.Eta() - H2.Eta())
      DMHH = abs(H1.M() - H2.M())
      DphiHH=  abs(deltaPhi(H1.Phi(), H2.Phi())) #  H1.DeltaPhi(H2)
    ###############
    # 1 H tag
    ###############
    elif(len(RecoFatJets2)>0 and len(RecoJetsBTag) > 1 ) : # 
        #print "1 tag"
        #if len(RecoJetsBTagCMS) > 3 : print "attention to potential lost"
        btagweight0 = (0.85*math.tanh(0.0025*RecoFatJets2[0].Pt()/2)*(25.0/(1+0.063*RecoFatJets2[0].Pt()/2)))**2
        category = 1 
        HTag1+=1*btagweight0
        H1 = RecoFatJets2[0]
        MH1 = RecoFatJets2Mass[0]
        ########## The other Higgs - by minimum mass difference with the fat H
        tomin = np.zeros((1000000))
        tomini = []
        tominj = []
        count=0
        for ii in range(0,len(RecoJetsBTag)): 
           for jj in range(ii+1,len(RecoJetsBTag)): 
                   tomin[count] = abs((RecoJetsBTag[ii]+RecoJetsBTag[jj]).M() - H1.M())
                   tomini.append(ii)
                   tominj.append(jj)
                   count+=1
        H2 = RecoJetsBTag[tomini[int(np.amin(tomin))]]+RecoJetsBTag[tominj[int(np.amin(tomin))]]
        MH2 = H2.M()
        XHMR = math.sqrt(((MH1 - 115)/24)**2 + ((MH2 - 115)/24)**2)
        PHH = (H1 + H2)
        costhh = abs(CosThetaStar(H1, PHH))
        costhh2 = abs(CosThetaStar(H2, PHH))
        PTHH = PHH.Pt()
        MHH = PHH.M()
        DRHH = H2.DeltaR( H1 )
        DetaHH = abs(H1.Eta() - H2.Eta())
        DMHH = abs(H1.M() - H2.M())
        DphiHH= deltaPhi(H1.Phi(), H2.Phi()) #  H1.DeltaPhi(H2)
        if(XHMR<1.0) : 
           HTag1sel+=1*btagweight0
           #if len(RecoJetsBTagCMS) > 3 : print "actually, no"
           recoHtag=1
    ###############
    # 0 H tag (with or without jet requirement)
    ###############
    elif ( len(RecoJetsBTagCMS) > 3 ) : # it is ok to have 1 fattag, if  and recoHtag==0
       #print "0 tag"
       category = 0 
       HTag0+=1
       if(len(RecoJets)==0) : HTag2noVLQ+=1
       ########## The H - by minimum mass difference
       tomin = np.zeros((100000000))
       tomini = []
       tominj = []
       tomink = []
       tominl = []
       H0jj = 0
       for jj in range(0,len(RecoJetsBTagCMS)):
          for kk in range(0,len(RecoJetsBTagCMS)):
            if (kk != jj) :
              for ii in range(0,len(RecoJetsBTagCMS)):
                if (ii != kk and ii!= jj) :
                  for yy in range(0,len(RecoJetsBTagCMS)):
                    if (yy!=ii and yy != kk and yy!= jj) :
                      # HMR
                      if ( H0jj==0 and RecoJetsBTagCMS[jj].DeltaR(RecoJetsBTagCMS[kk]) < 1.5 and RecoJetsBTagCMS[ii].DeltaR(RecoJetsBTagCMS[yy]) < 1.5) : 
                        H0jj+=1 # take only the first one , no ambiguities
                        if ((RecoJetsBTagCMS[jj]+RecoJetsBTagCMS[kk]).Pt() > (RecoJetsBTagCMS[ii]+RecoJetsBTagCMS[yy]).Pt()) :
                            H1 = (RecoJetsBTagCMS[jj]+RecoJetsBTagCMS[kk])
                            H2 = (RecoJetsBTagCMS[ii]+RecoJetsBTagCMS[yy])
                        else :   
                            H2 = (RecoJetsBTagCMS[jj]+RecoJetsBTagCMS[kk])
                            H1 = (RecoJetsBTagCMS[ii]+RecoJetsBTagCMS[yy])
       if H0jj ==1 :
         PHH = (H1 + H2)
         costhh = abs(CosThetaStar(H1, PHH))
         costhh2 = abs(CosThetaStar(H2, PHH))
         PTHH = PHH.Pt()
         MHH = PHH.M()
         DRHH = H2.DeltaR( H1 )
         DetaHH = abs(H1.Eta() - H2.Eta())
         DMHH = abs(H1.M() - H2.M())
         DphiHH= deltaPhi(H1.Phi(), H2.Phi()) #  H1.DeltaPhi(H2)
         if(H0jj==1):
           H0Tagjj+=1
           MH1 = H1.M()
           MH2 = H2.M()
           sigH = 23
           XHMR = math.sqrt(((H1.M() - 115)/sigH)**2 + ((H2.M() - 115)/sigH)**2)
           if(XHMR<1.0) : HTag0sel+=1
       # Doest it match Truth? - just one by object is ok
    ################
    # fill histos VLQ
    ###############
    #if(recoVLQ) :
    #    histVLQ1RecoMass.Fill(VLQ1.M())
    #    histVLQ2RecoMass.Fill(VLQ2.M())
    ################
    # fill histos H
    ###############
    # histograms to everithing
    histH1RecoMass.Fill(MH1)
    histH2RecoMass.Fill(MH2)
    histMHHRecoMass.Fill(MHH)
    if (MH1 > 0 and MH2 > 0 ) : histRedMHHRecoMass.Fill(MHH-MH1-MH2+250)
    histptHHReco.Fill(PTHH)
    #histHTRecoMass.Fill(branchHT.At(0).HT)
    if (MH1 > 0 and MH2 > 0 ) : histDRHHReco.Fill( math.sqrt(DetaHH*DetaHH+DphiHH*DphiHH)  ) # (DRHH)
    if (MH1 > 0 and MH2 > 0 ) : histDphiHHReco.Fill(   DphiHH   ) # (DRHH)
    if (MH1 > 0 and MH2 > 0 and math.sqrt(DetaHH*DetaHH+DphiHH*DphiHH) ==0 and DMHH ==0 and DetaHH ==0) : print category
    histDetaHHReco.Fill(DetaHH)
    histDMHHReco.Fill(DMHH)
    histCostHHReco.Fill(costhh)
    #histCostHH2Reco.Fill(costhh2)
    if(category == 0 and H0jj==1) : # If dijets pairs were found
           histH1RecoMassCat0.Fill(MH1)
           histH2RecoMassCat0.Fill(MH2)
           histMHHRecoMassCat0.Fill(MHH)
           histptHHRecoCat0.Fill(PTHH)
           histHTRecoMassCat0.Fill(branchHT.At(0).HT)
           histDRHHRecoCat0.Fill(DRHH)
           histDetaHHRecoCat0.Fill(DetaHH)
           histCostHHRecoCat0.Fill(costhh)
           #histCostHH2RecoCat0.Fill(costhh2)
           histNJetsNoH.Fill(len(RecoAllJets) -4)
    elif(category == 1) :
           histH1RecoMassCat1.Fill(MH1)
           histH2RecoMassCat1.Fill(MH2)
           histMHHRecoMassCat1.Fill(MHH)
           histptHHRecoCat1.Fill(PTHH)
           histHTRecoMassCat1.Fill(branchHT.At(0).HT)
           histDRHHRecoCat1.Fill(DRHH)
           histDetaHHRecoCat1.Fill(DetaHH)
           histCostHHRecoCat1.Fill(costhh)
           #histCostHH2RecoCat1.Fill(costhh2)
           histNJetsNoH.Fill(len(RecoAllJets) -3)
    elif(category == 2) :
           histH1RecoMassCat2.Fill(MH1)
           histH2RecoMassCat2.Fill(MH2)
           histMHHRecoMassCat2.Fill(MHH)
           histptHHRecoCat2.Fill(PTHH)
           histHTRecoMassCat2.Fill(branchHT.At(0).HT)
           histDRHHRecoCat2.Fill(DRHH)
           histDetaHHRecoCat2.Fill(DetaHH)
           histCostHHRecoCat2.Fill(costhh)
           #histCostHH2RecoCat2.Fill(costhh2)
           histNJetsNoH.Fill(len(RecoAllJets) -2)
    else : 
       lost+=1
       histMHHRecoMassCat2.Fill(MHH)
       histptHHRecoCat2.Fill(PTHH)

        #print " resolved jets "+ str(len(RecoJets))+ " btagged jets "+str(len(RecoJetsBTag))+" fat jets " +str(len(RecoFatJets2))
  #########################
  # output the efficiencies
  #########################
  print "The order is "+str(order)+" the mass is "+str(mass[1])+" GeV, the process is "+str(process)
  print "Categories (without mass selection)"
  print "mass(GeV) HTag2 HTag2sel HTag1 HTag1sel HTag0 HTag0sel lost negative numberOfEntries"
  efficiencies =  str(mass[1])+" "+str(HTag2)+" "+str(HTag2sel)+" "+str(HTag1)+" "+str(HTag1sel)+" "+str(HTag0)+" "+ str(HTag0sel)+" "+str(lost)+" "+ str(negative)+" "+str(numberOfEntries) 
      #str(HTag2truth)+" "+str(HTag1truth)+" "+str(HRes1truth)+" "+str(HRes0truth)
  print efficiencies
  print "saved efficiencies to the file: "+inputpath+"efficiency_"+str(order)+"_proc_"+str(process)+".txt"
  f = open(outputpath+"efficiency_"+str(order)+"_proc_"+str(process)+".txt", 'a+') 
  f.write(efficiencies)
  f.write('\n')
  f.close()
  print "Mass ATLASresolved4b ATLASresolvedDijet ATLASresolvedMass ATLASresolvedSigRegion "+\
      "ATLASboosted2J ATLASboostedDeta ATLASboostedXhh"+\
      "  CMSresolved4b  CMSresolved2H CMSresolvedSigRegion  CMSboosted2J  CMSboosted "+\
      "Neg Total"
  efficienciesATLASresolved = str(mass[1])+" "+str(ATLASresolved4b)+" "+str(ATLASresolvedDijet)+" "+str(ATLASresolvedMass)+" "+str(ATLASresolvedSigRegion)+" "+\
     str(ATLASboosted2J)+" "+str(ATLASboostedDeta)+" "+str(ATLASboostedXhh)+" "+\
     str(CMSresolved4b)+" "+str(CMSresolved2H)+" "+str(CMSresolvedSigRegion)+" "+str(CMSboosted2J)+" "+str(CMSboosted)+" "+\
     str(negative)+" "+str(numberOfEntries) 
  print efficienciesATLASresolved 
  fAr = open(outputpath+"efficiency_ATLAS_"+str(order)+"_proc_"+str(process)+".txt", 'a+') 
  fAr.write(efficienciesATLASresolved)
  fAr.write('\n')
  fAr.close()
  print "saved efficiencies of ATLAS analysis to the file: "+inputpath+"efficiency_ATLAS_"+str(order)+"_proc_"+str(process)+".txt"
  # add to a file
  print " "
  print "Categories (with mass selection)"
  #######################
  # save histos as image - test
  #####################
  #c1=ROOT.TCanvas("c2","c2",200,50,600,600)
  #histH1PT.Draw()
  #c1.Print("histH1PT.png")
  #c1.Clear()
  #######################
  # save histos as root file - to superimpose
  #####################
  f = ROOT.TFile(str(outputpath)+"histos_"+str(inputFile), 'RECREATE')
  histH1PT.Write()
  histH2PT.Write()
  histH1Mass.Write()
  histH2Mass.Write()
  histH1PTDR.Write()
  histH1PTDR.Write()
  histT1PT.Write()
  histT2PT.Write()
  histT1Mass.Write()
  histT2Mass.Write()
  histNJets.Write()
  histHTJets.Write()
  histNBJets.Write()
  histNFatJets.Write()
  histJ1Mass.Write()
  histFatJ1Mass.Write()
  histFatJ1Tau2.Write()
  histFatJ1Tau3.Write()
  #
  histXhh.Write() 
  histM4jATLAS.Write()
  histH1jjptATLAS.Write()
  histH2jjptATLAS.Write()
  histH1jjMATLAS.Write()
  histH2jjMATLAS.Write()
  #
  histXhhB.Write()
  histDetaB.Write() 
  histMJJATLAS.Write()
  histH1JptATLAS.Write()
  histH2JptATLAS.Write()
  histH1JMATLAS.Write()
  histH2JMATLAS.Write()
  #
  histNJetsNoH.Write()
  #
  histH1RecoMass.Write()
  histH2RecoMass.Write()
  histRedMHHRecoMass.Write()
  histH1RecoMassCat0.Write()
  histH2RecoMassCat0.Write()
  histH1RecoMassCat1.Write()
  histH2RecoMassCat1.Write()
  histH1RecoMassCat2.Write()
  histH2RecoMassCat2.Write()
  #
  histMHHRecoMass.Write()
  histMHHRecoMassCat2.Write()
  histMHHRecoMassCat1.Write()
  histMHHRecoMassCat0.Write()
  #
  histptHHReco.Write()
  histptHHRecoCat2.Write()
  histptHHRecoCat1.Write()
  histptHHRecoCat0.Write()
  #
  histHTRecoMass.Write()
  histHTRecoMassCat2.Write()
  histHTRecoMassCat1.Write()
  histHTRecoMassCat0.Write()
  #
  histDRHHReco.Write()
  histDMHHReco.Write()
  histDphiHHReco.Write()
  histDRHHRecoCat2.Write()
  histDRHHRecoCat1.Write()
  histDRHHRecoCat0.Write()
  #
  histDetaHHReco.Write()
  histDetaHHRecoCat2.Write()
  histDetaHHRecoCat1.Write()
  histDetaHHRecoCat0.Write()
  #
  histCostHHReco.Write()
  histCostHHRecoCat0.Write()
  histCostHHRecoCat1.Write()
  histCostHHRecoCat2.Write()
  #
  #histCostHH2Reco.Write()
  #histCostHH2RecoCat0.Write()
  #histCostHH2RecoCat1.Write()
  #histCostHH2RecoCat2.Write()
  #
  #histVLQ1RecoMass.Write()
  #histVLQ2RecoMass.Write()
  # CMS histograms
  histDetaCMS.Write() 
  histMJJCMS.Write()
  histMJJCMSred.Write()
  histXCMS.Write() 
  histM4bCMS.Write() 
  print "saved histograms to the file: "+str(outputpath)+"histos_"+str(inputFile)

if __name__ == "__main__":


  #inputpath="/afs/cern.ch/work/a/acarvalh/madanalysisVLQ/event/"
  inputpath="/afs/cern.ch/work/a/acarvalh/HH4b_HL-LHC/VLQNLO-DiHiggs-Pheno/Code/eos/cms/store/user/acarvalh/VLQ_hh/delphes_prun/"
  #inputpath = "~/eos/cms/store/user/acarvalh/VLQ_hh/delphes_prun/"
  outputpath="/afs/cern.ch/work/a/acarvalh/madanalysisVLQ/event/"
  #outputpath="events/"
  # NLO_proc_1_mvlq500.0_tagTH1_xsec_2.936.root

  main()
