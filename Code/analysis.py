#!/usr/bin/env python
# to run: ./analysis.py delphes_output.root
import os, sys, time,math
import ROOT
#from ROOT import TLatex,TPad,TList,TH1,TH1F,TH2F,TH1D,TH2D,TFile,TTree,TCanvas,TLegend,SetOwnership,gDirectory,TObject,gStyle,gROOT,TLorentzVector,TGraph,TMultiGraph,TColor,TAttMarker,TLine,TDatime,TGaxis,TF1,THStack,TAxis,TStyle,TPaveText,TAttFill,TF2, gPad, TGaxis, TChain,TClass 
from array import array
import numpy as np
pow = ROOT.TMath.Power
import bisect
from optparse import OptionParser
# Delphes headers
ROOT.gInterpreter.Declare('#include "ExRootAnalysis/ExRootTreeReader.h"')
ROOT.gInterpreter.Declare('#include "classes/DelphesClasses.h"')
# fastjet headers - to make prunned mass
ROOT.gInterpreter.Declare('#include "fastjet/PseudoJet.hh"')
ROOT.gInterpreter.Declare('#include "fastjet/ClusterSequence.hh"')
ROOT.gInterpreter.Declare('#include "fastjet/tools/Filter.hh"')
ROOT.gInterpreter.Declare('#include "fastjet/Selector.hh"')
ROOT.gInterpreter.Declare('#include "fastjet/tools/Pruner.hh"')
ROOT.gInterpreter.Declare('#include "fastjet/tools/MassDropTagger.hh"')
ROOT.gInterpreter.Declare('using namespace fastjet;')
#http://spartyjet.hepforge.org/
#from spartyjet import *

ROOT.gSystem.Load("libDelphes")
# Fastjet headers = just to sort by PT
#ROOT.fastjet
#from ROOT.fastjet import *
#ROOT.gInterpreter.Declare('#include "fastjet/PseudoJet.hh"')
# need to fix the path there
#import ExRootAnalysis
#from ExRootAnalysis import ExRootTreeReader
### https://cp3.irmp.ucl.ac.be/projects/delphes/wiki/WorkBook/RootTreeDescription

parser = OptionParser()
if len(sys.argv) < 1:
    print " Usage: Example1.py <input_file>"
    sys.exit(1)
inputFile = sys.argv[1]

'''
    print "   *** Aborting: the process ID must be an integer number"
    print "      - 1: p p > Q Qbar (QCD only)"
    print "      - 2: p p > Q Qbar (EW  only)"
    print "      - 3: p p > Q Qbar (EW-QCD interference - LO when Higgs coupling exists)"
    print "      - 4: p p > Q Q (EW only)"
    print "      - 5: p p > Qbar Qbar (EW only)"
    print "      - 6: p p -> Q + jets"
    print "      - 7: p p -> Qbar + jets"
'''

splited= inputFile.split('_')
mass =  splited[3].split('q')
XS =splited[6].split('.r')
order = splited[0]
process = splited[2]
print "The order is "+str(order)+" the mass is "+str(mass[1])+" GeV, the process is "+str(process)
VLQmass = float(mass[1])

inputpath="/afs/cern.ch/work/a/acarvalh/madanalysisVLQ/event/"
#########################
# Cuts
#########################
Tau21cut = 1
Tau31cut = 1
PrunMass2 = 100
PrunMass3 = 1000
VLQresolution = 200
Hresolution=50
#########################
# categorization
#########################
VLQTag2 = 0
VLQTag1 = 0
HTag2 = 0
HTag1 = 0
HTag0 = 0
#
# truth countings
#
VLQTag2truth = 0
#
VLQTag1truth = 0
HRes1truth = 0
VLQRes1truth = 0
#
HTag2truth = 0
HTag2VLQtruth = 0
#
HTag1truth = 0
HResHTag1truth = 0
VLQHTag1truth = 0
#
VLQRes0truth = 0
HRes0truth = 0
#
HTag2noVLQ = 0
HTag0noVLQ = 0
HTag1noVLQ = 0
HTag2noVLQtruth = 0
HTag0noVLQtruth = 0
HTag1noVLQtruth = 0
#
lost = 0
#
# ATLAS recast
ATLASboosted2J = 0
ATLASboostedDeta = 0
ATLASboostedXhh = 0
#
ATLASresolved4b = 0 
ATLASresolvedMass = 0
ATLASresolvedDijet = 0
ATLASresolvedSigRegion = 0 
#
lenQ=0
if (int(process) == 1 or int(process) == 2 or int(process) == 3 or int(process) == 4 or int(process) == 5 ) : lenQ = 2
elif (int(process) == 6 or int(process) == 7 ) : lenQ = 1
elif (int(process) == 10 ) : lenQ = 0
else : print "not implemented signal process"
#########################
# Create chain of root trees
#ROOT.gROOT.ProcessLine('.L struct1.C')
#s=ROOT.MyStruct_0_0()
chain = ROOT.TChain("Delphes")
chain.Add(str(inputpath)+str(inputFile))
#chain.SetMakeClass(1)
#chain.SetBranchAddress("ScalarHT", ROOT.AddressOf(s, "ScalarHT"))
# Create object of class ExRootTreeReader
treeReader = ROOT.ExRootTreeReader(chain)
numberOfEntries = treeReader.GetEntries() # 100 # 
print "The tree have "+str(numberOfEntries)+" events"
# Get pointers to branches used in this analysis
branchEvent = treeReader.UseBranch("Event")
branchJet = treeReader.UseBranch("Jet")
branchFatJet = treeReader.UseBranch("AK8Jet")
branchParticle = treeReader.UseBranch("Particle")
branchHT = treeReader.UseBranch("ScalarHT")
#to access constituents
branchEFlowTrack = treeReader.UseBranch("EFlowTrack")
branchTrack = treeReader.UseBranch("Track")
branchEFlowTower = treeReader.UseBranch("Tower")
branchEFlowTower = treeReader.UseBranch("EFlowNeutralHadron")
#############################################################
# Declare histograms
#############################################################
# genvariables
histH1PT = ROOT.TH1F("H1_pt", "Leading H Gen P_{T}", 100, 0.0, 3000.0)
histH1Mass = ROOT.TH1F("H1_mass", "Leading Gen M_{H}", 100, 40.0, 140.0)
histH2PT = ROOT.TH1F("H2_pt", "Sub-leading H Gen P_{T}", 100, 0.0, 3000.0)
histH2Mass = ROOT.TH1F("H2_mass", "Sub-leading  Gen M_{H}", 100, 40.0, 140.0)
#
histT1PT = ROOT.TH1F("T1_pt", "Leading Q Gen P_{T}", 100, 0.0, 3000.0)
histT1Mass = ROOT.TH1F("T1_mass", "Leading Gen M_{Q}", 100, 400.0, 3000.0)
histT2PT = ROOT.TH1F("T2_pt", "Sub-leading Q Gen P_{T}", 100, 0.0, 3000.0)
histT2Mass = ROOT.TH1F("T2_mass", "Sub-leading Gen M_{Q}", 100, 400.0, 3000.0)
#
histGenJ1PT = ROOT.TH1F("GenJ1_pt", "Leading GenJ P_{T}", 100, 0.0, 3000.0)
histGenJ2PT = ROOT.TH1F("GenJ2_pt", "Sub-leading GenJ P_{T}", 100, 0.0, 3000.0)
#
# global variables
#
histNJets = ROOT.TH1F("NJets", "# Jets (total)", 21, -0.5, 20.5)
histHTJets = ROOT.TH1F("HTJets", "HT Jets (total)", 100, 400.0, 3000.0)
histNFatJets = ROOT.TH1F("NFatJets", "# Fat Jets", 11, -0.5, 10.5)
histNBJets = ROOT.TH1F("NBJets", "#B Jets", 21, -0.5, 20.5)
#
# fat tag control plots - to do
#
histFatJ1Mass = ROOT.TH1F("FatJ1_mass", "Leading FatJ Mass", 100, 0.0, 3000.0)
histFatJ1Tau3 = ROOT.TH1F("FatJ1_tau3", "Leading FatJ #tau_{31}", 10, 0.0, 1)
histFatJ1Tau2 = ROOT.TH1F("FatJ1_tau2", "Leading FatJ #tau_{21}", 10, 0.0, 1)
#
# reconstructed masses - summing categories
#
histH1RecoMass = ROOT.TH1F("H1_mass_Reco", "Leading M_{H}", 100, 40.0, 1400.0)
histH2RecoMass = ROOT.TH1F("H2_mass_Reco", "Sub-leading  M_{H}", 100, 40.0, 1400.0)
histVLQ1RecoMass = ROOT.TH1F("VLQ1_mass_Reco", "Leading M_{Q}", 100, 40.0, 1400.0)
histVLQ2RecoMass = ROOT.TH1F("VLQ2_mass_Reco", "Sub-leading  M_{Q}", 100, 40.0, 1400.0)
# 
# Analysis variables
#
##########################################
#
# ATLAS histograms
#
histXhh = ROOT.TH1F("Xhh", "Xhh", 51, -0.5, 20.5) # just to check 
histM4jATLAS = ROOT.TH1F("histM4jATLAS", "histM4jATLAS", 51, 400, 2000) # find the variable binning
histH1jjptATLAS = ROOT.TH1F("histH1ptATLAS", "histH1ptATLAS", 51, 40, 2000) # just to see
histH2jjptATLAS = ROOT.TH1F("histH2ptATLAS", "histH2ptATLAS", 51, 40, 2000) # just to see
histH1jjMATLAS = ROOT.TH1F("histH1MATLAS", "histH1MATLAS", 51, 40, 300) # just to see
histH2jjMATLAS = ROOT.TH1F("histH2MATLAS", "histH2MATLAS", 51, 40, 300) # just to see
#
histXhhB = ROOT.TH1F("XhhB", "XhhB", 51, -0.5, 20.5) # just to check 
histDetaB = ROOT.TH1F("histDetaB", "histDetaB", 25, -0.5, 2.5) # just to check 
histMJJATLAS = ROOT.TH1F("histMJJATLAS", "histMJJATLAS", 51, 400, 2000) # find the variable binning
histH1JptATLAS = ROOT.TH1F("histH1JptATLAS", "histH1JptATLAS", 51, 40, 2000) # just to see
histH2JptATLAS = ROOT.TH1F("histH2JptATLAS", "histH2JptATLAS", 51, 40, 2000) # just to see
histH1JMATLAS = ROOT.TH1F("histH1JMATLAS", "histH1JMATLAS", 51, 40, 300) # just to see
histH2JMATLAS = ROOT.TH1F("histH2JMATLAS", "histH2JMATLAS", 51, 40, 300) # just to see
#
histJ1Mass = ROOT.TH1F("J1_mass", "Leading J Mass", 100, 0.0, 3000.0)
#############################################################
# Loop over all events
#############################################################
negative=0
for entry in range(0, numberOfEntries):
    # Load selected branches with data from specified event
    treeReader.ReadEntry(entry)
    weight = 1 #branchEvent.At(0).Weight
    #print weight
    #print "entry : "+str(entry)
    if(weight < 0 ) : 
        negative+=1
        continue
    #####################
    # Gen-level particles 
    #- it is wrong, taking the LHE information, we should use the genJets instead
    #####################
    Higgses = []
    Topone = []
    GenJets = []
    QQ = True
    if (QQ) :
        statusH = -1 # 1 #52# 22#52
        statusT = -1 #62 #62
        statusGenJ = -1 #62
    #print branchParticle.GetEntries()
    for part in range(0, branchParticle.GetEntries()):
       genparticle =  branchParticle.At(part)
       pdgCode = genparticle.PID
       #print pdgCode
       IsPU = genparticle.IsPU
       status = genparticle.M2 # genparticle.Status    
       # check if it is the correct status (for QQ the last 25 is 52 and the last topone 62)
       #print " pdgid "+ str(pdgCode)+" status "+str(status)
       if(IsPU == 0 and (pdgCode == 25)): # and status==statusH and abs(motherPID)  > 600000): 
          mother =  branchParticle.At(genparticle.M1)
          motherPID = mother.PID
          #print "H mother: "+str(motherPID)
          if(branchParticle.At(genparticle.D1).PID != 25) : 
             Higgses.append(genparticle) # find other way to follow
             if(entry ==0) :
                print "H decay: "+str(branchParticle.At(genparticle.D1).PID)
                print "H decay 2: "+str(branchParticle.At(genparticle.D2).PID)
       if (IsPU == 0 and (abs(pdgCode) > 6000000)): #and status==statusT ): 
          if(abs(branchParticle.At(genparticle.D1).PID) != abs(pdgCode) and len(Topone) <3) : 
             Topone.append(genparticle) # the LHE information...
             #print "Q decay: "+str(branchParticle.At(genparticle.D1).PID)
             #print "Q decay 2: "+str(branchParticle.At(genparticle.D2).PID)
       if (IsPU == 0 and (abs(pdgCode) == 5) and branchParticle.At(genparticle.M1).PID ==25):# there is no, Jet.Flavour works
          GenJets.append(genparticle)
          #print "b mother: "+str(genparticle.M1)
          #mother =  branchParticle.At(genparticle.M1)
          #motherPID = mother.PID
          #print " pdgid "+ str(pdgCode)+" status "+str(status)+" mother "+str(motherPID)
    #print len(Higgses)
    #print len(Topone)
    #print len(GenJets)
    if (len(Higgses) ==2) :
       # sort by pt = weird!
       if (Higgses[1].PT > Higgses[0].PT): 
          genparticle = Higgses[0]
          Higgses[0] = Higgses[1]
          Higgses[1] = genparticle
       histH1PT.Fill(Higgses[0].PT )
       histH2PT.Fill(Higgses[1].PT )
       histH1Mass.Fill(Higgses[0].Mass )
       histH2Mass.Fill(Higgses[1].Mass )
    else : print "not two Higggses " + str(len(Higgses))
    if (len(Topone) ==2) : # if there are 2
       if (Topone[1].PT > Topone[0].PT): 
          genparticle = Topone[0]
          Topone[0] = Topone[1]
          Topone[1] = genparticle    
       histT1PT.Fill(Topone[0].PT )
       histT2PT.Fill(Topone[1].PT )
       histT1Mass.Fill(Topone[0].Mass )
       histT2Mass.Fill(Topone[1].Mass )
    elif (len(Topone) ==1) :   
       histT1PT.Fill(Topone[0].PT )
       histT2PT.Fill(-100 )
       histT1Mass.Fill(Topone[0].Mass )
       histT2Mass.Fill(-100 )
    elif ((len(Topone)>2 and int(process)<6) or \
          (len(Topone)<1 and int(process) >5 and int(process) <9 ) or\
          (len(Topone)>0 and int(process)>9) ) : print "too much (few) Quark partners"
    #####################
    # Gen-Jets
    # it is wrong, taking the LHE information, we should use the genJets insteaddo not have btag 
    #####################
    # Jet.Flavour works
    #####################################################
    # Large-R for ATLAS boosted recast - those are sorted by PT 
    # Apply ATLAS algo - but do not remove from the other collections TOFIX
    #####################################################
    RecoFatJets2ATLAS = []
    for part in range(0, branchFatJet.GetEntries()): # add one more collection to the delphes card
        jet =  branchFatJet.At(part) # take the trimed jet 
        if( jet.PT > 250 and jet.PT < 1500 and abs(jet.Eta) < 2 and jet.Mass > 50 ) :
           dumb = ROOT.TLorentzVector()
           dumb.SetPtEtaPhiM(jet.PT,jet.Eta,jet.Phi,jet.Mass)
           RecoFatJets2ATLAS.append(dumb)
    ####################################################
    # Jets for signal categorization: We construct three excludent vectors of jets
    # 1) CMS-like fat jets (by now also for VLQ tag)
    # 2) b-tagged jets: also used for the ATLAS resolved recast
    # 3) non-btagged jets
    ####################################################
    #####################
    # Fat Jets - those are sorted by PT 
    # Apply CMS algo - TOFIX
    #####################
    #print "FatJet properties"
    RecoFatJets3 = [] # forget about it 
    RecoFatJets2 = []
    for part in range(0, branchFatJet.GetEntries()):
        jet =  branchFatJet.At(part)
        #particles = [ ROOT.PseudoJet ]
        #for const in range(0, jet.Constituents.GetEntriesFast()):
        #    momentum = ROOT.TLorentzVector()
        #    momentum.SetPxPyPzE(0.0, 0.0, 0.0, 0.0)
        #    object = jet.Constituents.At(0)
        #    if(isinstance(object, ROOT.Track) or isinstance(object, ROOT.Tower)) : 
        #       momentum += object.P4()
        #       particles.append(object.P4())
        #name = 'AntiKt4'
        #alg = ROOT.antikt_algorithm
        #       R = 0.4
        #       Rsb = 0.8
        #anti7def = ROOT.fastjet.JetDefinition(ROOT.FastJet.antikt_algorithm, 0.7)
        #CA(cambridge_algorithm, Rsb) = ROOT.JetDefinition

        dumb = ROOT.TLorentzVector()
        dumb.SetPtEtaPhiM(jet.PT,jet.Eta,jet.Phi,jet.Mass)
        JMass = jet.Prun[0] 
        #print " "
        #print jet.Prun[0]
        #print jet.SoftDrop[0]
        #print jet.SoftDroppedP4[0].M()
        if (int(process) == 10 ) : VLQmass = 7000
        else : VLQMass = float(mass[1])
        if (part==0) : 
            if(jet.Tau[1] >0 ) :
               if ( jet.Tau[3]/jet.Tau[1] > Tau31cut and JMass > VLQmass-VLQresolution ) :  RecoFatJets3.append(dumb)
               if ( jet.Tau[2]/jet.Tau[1] > Tau21cut ) : RecoFatJets2.append(dumb) # and JMass > PrunMass2 
               histFatJ1Mass.Fill(JMass) 
               histFatJ1Tau2.Fill(jet.Tau[2]/jet.Tau[1])
               histFatJ1Tau3.Fill(jet.Tau[3]/jet.Tau[1])
    
    #####################
    # Jets - those are sorted by PT 
    # making sure does do not match the FatJets
    #####################
    tolerancefat = 0.8
    tolerance = 0.4
    RecoJets = []
    RecoJetsBTag = []
    for part in range(0, branchJet.GetEntries()):
        jet =  branchJet.At(part)
        dumb = ROOT.TLorentzVector()
        dumb.SetPtEtaPhiM(jet.PT,jet.Eta,jet.Phi,jet.Mass)
        test = 0
        for ii in range(0, len(RecoFatJets2)):
          if ( dumb.DeltaR(RecoFatJets2[ii]) < tolerancefat ) : test = -1
        if ( test == 0 and jet.BTag == 1 and jet.PT >40 and abs(jet.Eta) <2.5 ) : RecoJetsBTag.append(dumb)
        elif ( test == 0 ) : RecoJets.append(dumb) 
        #print "btag "+str(jet.BTag)
        #print "btag algo "+str(jet.BTagAlgo)
        if (part==0) : 
            histJ1Mass.Fill(jet.Mass)
    ########################################################
    # Global variables
    ########################################################
    histNJets.Fill(branchJet.GetEntries())
    histHTJets.Fill(branchHT.At(0).HT)
    histNBJets.Fill(len(RecoJetsBTag))
    histNFatJets.Fill(len(RecoFatJets2))
    ########################################################
    # Algoritm for recast ATLAS resolved
    ########################################################
    if(len(RecoJetsBTag) > 3) :
        ATLASresolved4b+=1
        passcut = 0
        # the jets are already organized by Pt
        counter = 0
        counter2 = 0
        mjj1 = -10
        mjj2 = -10
        ptjj1 = -10
        ptjj2 = -10
        etajj1 = -10
        etajj2 = -10
        for jj in range(0,len(RecoJetsBTag)):
          for kk in range(0,len(RecoJetsBTag)):
            if (kk != jj) :
              for ii in range(0,len(RecoJetsBTag)):
                if (ii != kk and ii!= jj) :
                  for yy in range(0,len(RecoJetsBTag)):
                    if (yy!=ii and yy != kk and yy!= jj) :
                      if ( RecoJetsBTag[jj].DeltaR(RecoJetsBTag[kk]) < 1.5 and RecoJetsBTag[ii].DeltaR(RecoJetsBTag[yy]) < 1.5) : 
                        if ((RecoJetsBTag[jj]+RecoJetsBTag[kk]).Pt() > (RecoJetsBTag[ii]+RecoJetsBTag[yy]).Pt()) :
                            j1 = (RecoJetsBTag[jj]+RecoJetsBTag[kk])
                            j2 = (RecoJetsBTag[ii]+RecoJetsBTag[yy])
                        else :   
                            j2 = (RecoJetsBTag[jj]+RecoJetsBTag[kk])
                            j1 = (RecoJetsBTag[ii]+RecoJetsBTag[yy])
                            counter+=1 # take only the first one , no ambiguities
                        if (counter == 0 and j1.Pt() > 200 and j2.Pt() >150) : 
                            mjj1 = j1.M()
                            mjj2 = j2.M()
                            ptjj1 = j1.Pt()
                            ptjj2 = j2.Pt()
                            counter2+=1
            #if(counter>0) : print "there was ambiguity"
        #if (counter==0) : print "opsy, made no pairs" 
        if (counter2==0) : passcut = 1
        if(passcut==0) : ATLASresolvedDijet+=1
        m4b = (RecoJetsBTag[0]+RecoJetsBTag[1]+RecoJetsBTag[2]+RecoJetsBTag[3]).M()
        ### leading jet
        if(m4b > 910) : 
           if ( ptjj1 < 400 ) :  passcut = 1
        elif(m4b < 600) : 
           if ( ptjj1 < 200 ) :  passcut = 1
        else : 
           if ( ptjj1 < 0.65*m4b-190 ) :  passcut = 1
        ### subleading jet
        if(m4b > 990) : 
            if ( ptjj2 < 400 ) :  passcut = 1
        elif(m4b < 520) : 
            if ( ptjj2 < 200 ) :  passcut = 1
        else : 
            if ( ptjj2 < 0.23*m4b+30 ) :  passcut = 1
        ### angle
        if(m4b < 810) :
            if ( abs(etajj1 - etajj1)< 1.0 and etajj1!=-10) :  passcut = 1
        else :
            if ( abs(etajj1 - etajj1)< 0.0016*m4b-0.28 and etajj1!=-10 ) :  passcut = 1
        # We ignore the ttbar veto (90% efficiency)
        if (passcut==0) : 
           ATLASresolvedMass+=1
           Xhh = math.sqrt( ((mjj1-124)**2/(0.1*mjj1)**2) + ((mjj2-115)**2/(0.1*mjj2)**2)) 
           histXhh.Fill(Xhh)
           if (Xhh < 1.6 ) : 
               ATLASresolvedSigRegion+=1
               histM4jATLAS.Fill(m4b)
               histH1jjptATLAS.Fill(ptjj1)
               histH2jjptATLAS.Fill(ptjj2)
               histH1jjMATLAS.Fill(mjj1)
               histH2jjMATLAS.Fill(mjj2)
    ########################################################
    # Algoritm for recast ATLAS boosted
    ########################################################
    # large-R jets : RecoFatJets2ATLAS
    if(len(RecoFatJets2ATLAS) > 1) :
      if(RecoFatJets2ATLAS[0].Pt() >350 ):
        ATLASboosted2J+=1
        Deta = abs(RecoFatJets2ATLAS[0].Eta()-RecoFatJets2ATLAS[1].Eta()) < 1.7
        histDetaB.Fill(Deta)
        if (Deta < 1.7) :
           ATLASboostedDeta+=1
           XhhB = math.sqrt( ((RecoFatJets2ATLAS[0].M()-124)**2/(0.1*RecoFatJets2ATLAS[0].M())**2) + ((RecoFatJets2ATLAS[1].M()-115)**2/(0.1*RecoFatJets2ATLAS[1].M())**2)) 
           histXhhB.Fill(XhhB)
           if(XhhB < 1.6 ) : 
              ATLASboostedXhh+=1
              histMJJATLAS.Fill((RecoFatJets2ATLAS[0]+RecoFatJets2ATLAS[1]).M())
              histH1JptATLAS.Fill(RecoFatJets2ATLAS[0].Pt())
              histH2JptATLAS.Fill(RecoFatJets2ATLAS[1].Pt())
              histH1JMATLAS.Fill(RecoFatJets2ATLAS[0].M())
              histH2JMATLAS.Fill(RecoFatJets2ATLAS[1].M())
    ########################################################
    # Algoritm for categorization 
    ########################################################
    VLQ1 = ROOT.TLorentzVector()
    VLQ2 = ROOT.TLorentzVector()
    H1 = ROOT.TLorentzVector()
    H2 = ROOT.TLorentzVector()
    ###############
    # 2 VLQ tag
    ###############
    if(len(RecoFatJets3)>1) :
       VLQTag2+=1
       VLQ1 = RecoFatJets3[0]
       VLQ2 = RecoFatJets3[1]
       # Take the H1 and H2 from the subjets
       # Doest it match Truth? - up to one
       count =0
       for ii in range(0,2): 
           dumb = ROOT.TLorentzVector()
           dumb.SetPtEtaPhiM(Topone[ii].Px,Topone[ii].Eta,Topone[ii].Phi,Topone[ii].Mass)
           for jj in range(0,lenQ):
               #print str(Topone[ii].Mass)+" "+str(dumb.M()) +" "+ str(RecoFatJets3[jj].M())
               if ( RecoFatJets3[jj].DeltaR( dumb ) < tolerance) : count+=1
       if(count > 0) : VLQTag2truth+=1
       if(count > 2) : print "to much truth in 2 VLQ tag" 
    ###############
    # 1 VLQ tag
    ###############
    elif(len(RecoFatJets3)>0 and len(RecoJetsBTag) > 1 and len(RecoJets) > 0 ) :
       VLQTag1+=1
       VLQ1 = RecoFatJets3[0]
       # take the H from the subjets?
       # the other 'VLQ' - take the VLQ by minimum invariant mass differnce (the H will come from it)
       # not sure it will work for QH
       tomin = np.zeros((1000))
       tomini = []
       tominj = []
       tomink = []
       count=0
       #print str(minpos)+" "+str(len(RecoFatJets3))+" "+str(len())
       for ii in range(0,len(RecoJets)): 
            for jj in range(0,len(RecoJetsBTag)):
                for kk in range(jj+1,len(RecoJetsBTag)):
                #if (RecoJets[ii].DeltaR(VLQ1) > tolerancefat and RecoJetsBTag[jj].DeltaR(VLQ1) > tolerancefat and RecoJetsBTag[kk].DeltaR(VLQ1) > tolerancefat ) :
                        tomin[count] = abs((RecoJets[ii]+RecoJetsBTag[jj]+RecoJetsBTag[kk]).M() - VLQ1.M())
                        tomini.append(ii)
                        tominj.append(jj)
                        tomink.append(kk)
                        count+=1
       minpos = int(np.amin(tomin))
       #print str(minpos)+" "+str(len(tominj))
       #       print str(tominj[minpos])+" "+str(tomink[minpos])
       H2 = RecoJetsBTag[tominj[minpos]]+RecoJetsBTag[tomink[minpos]]
       VLQ2 = RecoJets[tomini[minpos]]+H2
       # Doest it match Truth?
       count =0
       count2 =0
       count3 =0
       for ii in range(0,2): 
            dumb2 = ROOT.TLorentzVector()
            dumb2.SetPtEtaPhiM(Higgses[ii].Px,Higgses[ii].Eta,Higgses[ii].Phi,Higgses[ii].Mass)
            if ( H2.DeltaR( dumb2 ) < tolerance) : count3+=1
       if(count3 > 0) : HResHTag1truth+=1
       if(count3 > 1) : print "to much truth in 1 VLQ tag, H resolved" 
       for ii in range(0,lenQ): 
            dumb = ROOT.TLorentzVector()
            dumb.SetPtEtaPhiM(Topone[ii].Px,Topone[ii].Eta,Topone[ii].Phi,Topone[ii].Mass)
            if ( VLQ1.DeltaR( dumb ) < tolerancefat) : count+=1
            if ( VLQ2.DeltaR( dumb ) < tolerance) : count2+=1
       if(count > 0) : VLQTag1truth+=1
       if(count > 1) : print "to much truth in 1 VLQ tag" 
       if(count2 > 0) : VLQRes1truth1+=1
       if(count2 > 1) : print "to much truth in 1 VLQ tag, VLQ resolved" 
    ###############
    # 2 H tag
    ###############
    elif(len(RecoFatJets2)>1 and len(RecoJets)>1) :
       HTag2+=1
       H1 = RecoFatJets2[0]
       H2 = RecoFatJets2[1]
       ########## The VLQs - pair with the jets by minimum invariant mass differnce 
       tomin1 = np.zeros((1000000))
       tomini = []
       tominj = []
       count=0
       for ii in range(0,len(RecoJets)): 
           for jj in range(ii,len(RecoJets)): 
                # make sure RocoJets do not match a fat jet
                if (RecoJets[ii].DeltaR(H1) > tolerance and RecoJets[ii].DeltaR(H2) > tolerance  and RecoJets[jj].DeltaR(H1) > tolerance and RecoJets[jj].DeltaR(H2) > tolerance ) :
                    #print abs((RecoJets[ii]+H1).M() - (RecoJets[jj]+H2).M())
                   tomin1[count] = abs((RecoJets[ii]+H1).M() - (RecoJets[jj]+H2).M())
                   tomini.append(ii)
                   tominj.append(jj)
                   count+=1
       VLQ1 = H1+RecoJets[tomini[int(0)]] # np.amin(tomin1)
       VLQ2 = H2+RecoJets[tominj[int(0)]] # np.amin(tomin1)                                
       # Doest it match Truth? - only one H is enought 
       count =0
       count2 =0
       for ii in range(0,len(Higgses)): 
            dumb2 = ROOT.TLorentzVector()
            dumb2.SetPtEtaPhiM(Higgses[ii].Px,Higgses[ii].Eta,Higgses[ii].Phi,Higgses[ii].Mass)
            if ( H1.DeltaR( dumb ) < tolerancefat) : count+=1
            if ( H2.DeltaR( dumb ) < tolerancefat) : count+=1
       if(count > 0) : HTag2truth+=1 # print "to much truth, 2H "+str(count) 
       if(count > 2) : print "to much truth in 2 H tag" 
       for ii in range(0,lenQ): 
            dumb = ROOT.TLorentzVector()
            dumb.SetPtEtaPhiM(Topone[ii].Px,Topone[ii].Eta,Topone[ii].Phi,Topone[ii].Mass)
            if ( VLQ1.DeltaR( dumb ) < tolerance) : count2+=1
            if ( VLQ2.DeltaR( dumb ) < tolerance) : count2+=1
       if(count2 > 0) : HTag2VLQtruth+=1 # print "to much truth, 2H "+str(count) 
       if(count2 > 2) : print "to much truth in 2 H tag, for VLQ reco" 
    ###############
    # 1 H tag
    ###############
    elif(len(RecoFatJets2)>0 and len(RecoJetsBTag) > 1 and len(RecoJets) > 1) : # 
       HTag1+=1
       H1 = RecoFatJets2[0]
       ########## The other Higgs - by minimum mass difference with the fat H
       tomin = np.zeros((1000000))
       tomini = []
       tominj = []
       count=0
       for ii in range(0,len(RecoJetsBTag)): 
           for jj in range(ii+1,len(RecoJetsBTag)): 
                # make sure RocoJets do not match a fat jet
                #if (RecoJetsBTag[ii].DeltaR(H1) > tolerance and RecoJetsBTag[jj].DeltaR(H1) > tolerance ) :
                    #print abs((RecoJetsBTag[ii]+RecoJetsBTag[jj]).M() - H1.M())
                   tomin[count] = abs((RecoJetsBTag[ii]+RecoJetsBTag[jj]).M() - H1.M())
                   tomini.append(ii)
                   tominj.append(jj)
                   count+=1
       H2 = RecoJetsBTag[tomini[int(np.amin(tomin))]]+RecoJetsBTag[tominj[int(np.amin(tomin))]]
       ########## The VLQs - pair with the jets by minimum invariant mass differnce 
       tomin = np.zeros((1000000))
       tomini = []
       tominj = []
       count=0
       for ii in range(0,len(RecoJets)): 
           for jj in range(ii+1,len(RecoJets)): 
                # make sure RocoJets do not match the fat jet
                if (RecoJets[ii].DeltaR(H1) > tolerancefat and RecoJets[jj].DeltaR(H1) > tolerancefat ) :
                    tomin[count] = abs((RecoJets[ii]+H1).M() - (RecoJets[jj]+H2).M())
                    tomini.append(ii)
                    tominj.append(jj)
                    count+=1
       VLQ1 = H1+RecoJets[tomini[int(np.amin(tomin))]]
       VLQ2 = H2+RecoJets[tominj[int(np.amin(tomin))]] 
       ######### Doest it match Truth? - just one VLQ is enought
       count =0
       count2 =0
       count3 =0
       for ii in range(0,2): 
            dumb2 = ROOT.TLorentzVector()
            dumb2.SetPtEtaPhiM(Higgses[ii].Px,Higgses[ii].Eta,Higgses[ii].Phi,Higgses[ii].Mass)
            if ( H1.DeltaR( dumb ) < tolerancefat) : count+=1
            if ( H2.DeltaR( dumb ) < tolerance) : count2+=1
       if(count > 0) : HTag1truth +=1  
       if(count > 1) : print "to much truth resolved 1 H tag " 
       if(count2 > 0) : HResHTag1truth+=1  
       if(count2 > 1) : print "to much truth, H resolved 1 H tag " 
       for ii in range(0,lenQ): 
           dumb = ROOT.TLorentzVector()
           dumb.SetPtEtaPhiM(Topone[ii].Px,Topone[ii].Eta,Topone[ii].Phi,Topone[ii].Mass)
           if ( VLQ1.DeltaR( dumb ) < tolerance) : count3+=1
       if(count3 > 0) : VLQHTag1truth+=1  
       if(count3 > 1) : print "to much truth, VLQ resolved 1 H tag " 
    ###############
    # 0 H tag (with ot without jet requirement)
    ###############
    elif ( len(RecoJetsBTag) > 3) :
       if(len(RecoJets)==0) : HTag2noVLQ+=1
       ########## The H - by minimum mass difference
       tomin = np.zeros((100000000))
       tomini = []
       tominj = []
       tomink = []
       tominl = []
       count=0
       #print str(len(RecoJets))+" and "+str(len(RecoJetsBTag))
       for ii in range(0,len(RecoJetsBTag)): 
            for jj in range(ii+1,len(RecoJetsBTag)): 
                for kk in range(jj+1,len(RecoJetsBTag)):
                    for ll in range(kk+1,len(RecoJetsBTag)): 
                       tomin[count] = abs((RecoJetsBTag[ii]+RecoJetsBTag[jj]).M() - (RecoJetsBTag[kk]+RecoJetsBTag[ll]).M())
                       tomini.append(ii)
                       tominj.append(jj)
                       tomink.append(kk)
                       tominl.append(ll)
                       count+=1
       H1 = RecoJetsBTag[tomini[int(np.amin(tomin))]]+RecoJetsBTag[tominj[int(np.amin(tomin))]]
       H2 = RecoJetsBTag[tomink[int(np.amin(tomin))]]+RecoJetsBTag[tominl[int(np.amin(tomin))]]
       # Doest it match Truth? - just one by object is ok
       count =0
       count2 =0
       for ii in range(0,len(Higgses)): 
            dumb2 = ROOT.TLorentzVector()
            dumb2.SetPtEtaPhiM(Higgses[ii].Px,Higgses[ii].Eta,Higgses[ii].Phi,Higgses[ii].Mass)
            if ( H1.DeltaR( dumb ) < tolerance) : count+=1
            if ( H2.DeltaR( dumb ) < tolerance) : count+=1
            if(count > 0 and len(RecoJets)>1) : HRes0truth +=1  
            elif (count > 0 ) : HTag0noVLQtruth+=1
       if(count > 2) : print "to much truth resolved 1 H tag " 
       if(len(RecoJets)>1) :
           HTag0+=1
           ########## The VLQs - pair with the jets by minimum invariant mass differnce
           tomin = np.zeros((1000))
           tomini = []
           tominj = []
           count=0
           for ii in range(0,len(RecoJets)): 
               for jj in range(ii,len(RecoJets)): 
                  tomin[count] = abs((RecoJets[ii]+H1).M() - (RecoJets[jj]+H2).M())
                  tomini.append(ii)
                  tominj.append(jj)
                  count+=1
           VLQ1 = H1+RecoJets[tomini[int(np.amin(tomin))]]
           VLQ2 = H2+RecoJets[tominj[int(np.amin(tomin))]]   
           # does it match the truth?
           for ii in range(0,lenQ): 
               dumb = ROOT.TLorentzVector()
               dumb.SetPtEtaPhiM(Topone[ii].Px,Topone[ii].Eta,Topone[ii].Phi,Topone[ii].Mass)
               if ( VLQ1.DeltaR( dumb ) < tolerance) : count2+=1
               if(count2 > 0) : VLQRes0truth+=1  
           if(count2 > 2) : print "to much truth, VLQ resolved 1 H tag " 
       ################
       # fill histos
       ###############
       histVLQ1RecoMass.Fill(VLQ1.M())
       histVLQ2RecoMass.Fill(VLQ2.M())
       ###############
       # 2 H tag, no jet requirement
       ###############
       if(len(RecoFatJets2)>2) :
           HTag2noVLQ+=1
           H1 = RecoFatJets2[0]
           H2 = RecoFatJets2[1]
           # Take the H1 and H2 from the subjets
           # Doest it match Truth? - up to one
           count =0
           for ii in range(0,2): 
               dumb = ROOT.TLorentzVector()
               dumb.SetPtEtaPhiM(Higgses[ii].Px,Higgses[ii].Eta,Higgses[ii].Phi,Higgses[ii].Mass)
               for jj in range(0,lenQ):
                    #print str(Topone[ii].Mass)+" "+str(dumb.M()) +" "+ str(RecoFatJets3[jj].M())
                    if ( RecoFatJets2[jj].DeltaR( dumb ) < tolerance) : count+=1
               if(count > 0) : HTag2noVLQtruth +=1
               if(count > 2) : print "to much truth in 2 H tag, no VLQ" 
       ###############
       # 1 H tag , no jet requirement
       ###############
       elif(len(RecoFatJets2)>0 and len(RecoJetsBTag) > 1 ) :
            HTag1noVLQ+=1
            H1 = RecoFatJets2[0]
            # take the H from the subjets?
            # the other 'VLQ' - take the VLQ by minimum invariant mass differnce (the H will come from it)
            # not sure it will work for QH
            tomin = [float(100000)]*1000 #np.zeros((1000))
            tominj = []
            tomink = []
            count=0
            #print str(minpos)+" "+str(len(RecoFatJets3))+" "+str(len())
            for jj in range(0,len(RecoJetsBTag)):
                for kk in range(jj+1,len(RecoJetsBTag)):
                #if (RecoJets[ii].DeltaR(VLQ1) > tolerancefat and RecoJetsBTag[jj].DeltaR(VLQ1) > tolerancefat and RecoJetsBTag[kk].DeltaR(VLQ1) > tolerancefat ) :
                    tomin[count] = abs((RecoJetsBTag[jj]+RecoJetsBTag[kk]).M() - H1.M())
                    tominj.append(jj)
                    tomink.append(kk)
                    count+=1
            minpos = int(np.amin(tomin))
            # FIXME
            #print str(len(RecoJetsBTag))+" "+str(minpos)+" "+str(len(tominj))+" "+" "+str(len(tomink))+" "+str(tomin[minpos])
            #print str(tominj[minpos])+" "+str(tomink[minpos])
            #H2 = RecoJetsBTag[tominj[minpos]]+RecoJetsBTag[tomink[minpos]]
            # Doest it match Truth?
            count =0
            count2 =0
            count3 =0
            for ii in range(0,2): 
                dumb2 = ROOT.TLorentzVector()
                dumb2.SetPtEtaPhiM(Higgses[ii].Px,Higgses[ii].Eta,Higgses[ii].Phi,Higgses[ii].Mass)
                if ( H2.DeltaR( dumb2 ) < tolerance) : count3+=1
            if(count3 > 0) : HTag1noVLQtruth+=1
            if(count3 > 1) : print "to much truth in 1 H tag without jet requirement, H resolved" 
       ################
       # fill histos
       ###############
       histH1RecoMass.Fill(H1.M())
       histH2RecoMass.Fill(H2.M())


       ########################################################
       # Analysis variables
       ########################################################
       # a 2D plot - medium mass and min difference
       #
       ########################################################
       # Mass selection
       ########################################################
       #

    else : 
       lost+=1
        #print " resolved jets "+ str(len(RecoJets))+ " btagged jets "+str(len(RecoJetsBTag))+" fat jets " +str(len(RecoFatJets2))

#########################
# output the efficiencies
#########################
print "The order is "+str(order)+" the mass is "+str(mass[1])+" GeV, the process is "+str(process)
print "Categories (without mass selection)"
print "Mass VLQTag2 VLQTag1 HTag2 HTag1 HTag0 HTag2noVLQ HTag1noVLQ HTag0noVLQ lost Neg Total " +\
      "VLQTag2truth VLQTag1truth HRes1truth VLQRes1truth "+\
      "HTag2truth  HTag2VLQtruth "+\
      "HTag1truth HResHTag1truth VLQHTag1truth "+\
      "VLQRes0truth HRes0truth HTag2noVLQtruth HTag1noVLQtruth HTag0noVLQtruth"
efficiencies =  str(mass[1])+" "+str(VLQTag2)+" "+str(VLQTag1)+" "+str(HTag2)+" "+str(HTag1)+" "+str(HTag0)+" "+str(lost)+" "+\
      str(HTag2noVLQ)+" "+str(HTag1noVLQ)+" "+str(HTag0noVLQ)+" "+\
      str(negative)+" "+str(numberOfEntries)+ " "+ \
      str(VLQTag2truth)+" "+str(VLQTag1truth)+" "+str(HRes1truth)+" "+str(VLQRes1truth)+" "+\
      str(HTag2truth)+" "+str(HTag2VLQtruth)+" "+\
      str(HTag1truth)+" "+str(HResHTag1truth)+" "+str(VLQHTag1truth)+" "+\
      str(VLQRes0truth)+" "+str(HRes0truth)+" "+str(HTag2noVLQtruth)+" "+str(HTag1noVLQtruth)+" "+str(HTag0noVLQtruth)
print efficiencies
print "saved efficiencies to the file: "+inputpath+"efficiency_"+str(order)+"_proc_"+str(process)+".txt"
f = open(inputpath+"efficiency_"+str(order)+"_proc_"+str(process)+".txt", 'a+') 
f.write(efficiencies)
f.write('\n')
f.close()

print "Mass ATLASresolved4b ATLASresolvedDijet ATLASresolvedMass ATLASresolvedSigRegion "+\
      "ATLASboosted2J ATLASboostedDeta ATLASboostedXhh"+\
      "Neg Total"
efficienciesATLASresolved = str(mass[1])+" "+str(ATLASresolved4b)+" "+str(ATLASresolvedDijet)+" "+str(ATLASresolvedMass)+" "+str(ATLASresolvedSigRegion)+" "+\
str(ATLASboosted2J)+" "+str(ATLASboostedDeta)+" "+str(ATLASboostedXhh)+" "+\
str(negative)+" "+str(numberOfEntries) 
print efficienciesATLASresolved 
fAr = open(inputpath+"efficiency_ATLAS_"+str(order)+"_proc_"+str(process)+".txt", 'a+') 
fAr.write(efficienciesATLASresolved)
fAr.write('\n')
fAr.close()
print "saved efficiencies of ATLAS analysis to the file: "+inputpath+"efficiency_ATLAS_"+str(order)+"_proc_"+str(process)+".txt"

# add to a file
print " "
print "Categories (with mass selection)"
#######################
# save histos as image - test
#####################
#c1=ROOT.TCanvas("c2","c2",200,50,600,600)
#histH1PT.Draw()
#c1.Print("histH1PT.png")
#c1.Clear()
#######################
# save histos as root file - to superimpose
#####################
f = ROOT.TFile(str(inputpath)+"histos_"+str(inputFile), 'RECREATE')
histH1PT.Write()
histH2PT.Write()
histH1Mass.Write()
histH2Mass.Write()
histT1PT.Write()
histT2PT.Write()
histT1Mass.Write()
histT2Mass.Write()
histNJets.Write()
histHTJets.Write()
histNBJets.Write()
histNFatJets.Write()
histJ1Mass.Write()
histFatJ1Mass.Write()
histFatJ1Tau2.Write()
histFatJ1Tau3.Write()
histH1RecoMass.Write()
histH2RecoMass.Write()
histVLQ1RecoMass.Write()
histVLQ2RecoMass.Write()
# ATLAS resolved
histXhh.Write()
histM4jATLAS.Write()
histH1jjptATLAS.Write()
histH2jjptATLAS.Write()
histH1jjMATLAS.Write()
histH2jjMATLAS.Write()
# ATLAS boosted
histXhhB.Write()
histDetaB.Write()
histMJJATLAS.Write()
histH1JptATLAS.Write()
histH2JptATLAS.Write()
histH1JMATLAS.Write()
histH2JMATLAS.Write()
#histGenJ1PT.Write()
#histGenJ2PT.Write()
f.Write()
f.Close()
print "saved histograms to the file: "+str(inputpath)+"histos_"+str(inputFile)
