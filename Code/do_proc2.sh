#!/bin/bash
./analysis_recoHH.py NLO_proc_2_mvlq500.0_tagTH1_xsec_0.001563.root &
./analysis_recoHH.py NLO_proc_2_mvlq650.0_tagTH1_xsec_0.001309.root &
./analysis_recoHH.py NLO_proc_2_mvlq800.0_tagTH1_xsec_0.001045.root &
./analysis_recoHH.py NLO_proc_2_mvlq1000.0_tagTH1_xsec_0.0007364.root &  
./analysis_recoHH.py NLO_proc_2_mvlq2000.0_tagTH1_xsec_7.988e-05.root & 
./analysis_recoHH.py NLO_proc_2_mvlq3000.0_tagTH1_xsec_5.544e-06.root & 

./analysis_recoHH.py LO_proc_2_mvlq500.0_tagTH1_xsec_0.001371.root &   
./analysis_recoHH.py LO_proc_2_mvlq650.0_tagTH1_xsec_0.001129.root &
./analysis_recoHH.py LO_proc_2_mvlq800.0_tagTH1_xsec_0.000892.root &
./analysis_recoHH.py LO_proc_2_mvlq1000.0_tagTH1_xsec_0.000626.root &  
./analysis_recoHH.py LO_proc_2_mvlq2000.0_tagTH1_xsec_6.225e-05.root & 
./analysis_recoHH.py LO_proc_2_mvlq3000.0_tagTH1_xsec_4.094e-06.root &
